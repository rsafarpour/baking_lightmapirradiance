#pragma once

#define NOMINMAX
#include <Windows.h>

using fn_wheel = void(*)();

HWND open_window(LPCWSTR title);
void close_window(HWND window);

void set_wheelfunctions(fn_wheel up, fn_wheel down);