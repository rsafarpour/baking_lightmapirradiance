#include "window.h"
#define NOMINMAX
#include <Windows.h>
#include <Windowsx.h>
#include <algorithm>

//float yaw_angle = 0.0f;
//float pitch_angle = 0.0f;
//float x_translation = 0.0f;
//float y_translation = 0.0f;
//float z_translation = -2.0f;

void wheelnull() {};

fn_wheel wheel_up   = wheelnull;
fn_wheel wheel_down = wheelnull;

LRESULT CALLBACK main_window_handler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    int wheel_val = GET_WHEEL_DELTA_WPARAM(wParam);
    int mpos_x = GET_X_LPARAM(lParam);
    int mpos_y = GET_Y_LPARAM(lParam);
    switch (message)
    {
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;
        break;
    case WM_MOUSEWHEEL:
        if (wheel_val > 0) { wheel_up(); }
        if (wheel_val < 0) { wheel_down(); }
    
    //    z_translation = std::max(std::min(0.0f, z_translation), -10.0f);
    //    camera.SetPosition({ x_translation, y_translation, z_translation });
        break;
    //case WM_MOUSEMOVE:
    //    if (mpos_x < 200) { x_translation += 0.1f; }
    //    if (mpos_x > 600) { x_translation -= 0.1f; }
    //    if (mpos_y < 200) { y_translation -= 0.1f; }
    //    if (mpos_y > 400) { y_translation += 0.1f; }
    //
    //    x_translation = std::max(std::min(5.0f, x_translation), -5.0f);
    //    y_translation = std::max(std::min(5.0f, y_translation), -5.0f);
    //    camera.SetPosition({ x_translation, y_translation, z_translation });
    //    break;
    }

    return DefWindowProc(hWnd, message, wParam, lParam);
}

HWND open_window(LPCWSTR title) {
    WNDCLASSEX wc = {};

    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = main_window_handler;
    wc.hInstance = GetModuleHandle(nullptr);
    wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
    wc.lpszClassName = L"MainWindow";

    if (RegisterClassEx(&wc) != 0) {
        HWND hwnd = CreateWindowEx(0,
            L"MainWindow",
            title,
            WS_OVERLAPPEDWINDOW,
            300,
            300,
            800,
            600,
            nullptr,
            nullptr,
            wc.hInstance,
            nullptr);
        if (hwnd != nullptr) {
            ShowWindow(hwnd, 10);
            return hwnd;
        }
    }

    MessageBox(nullptr, TEXT("Failed opening main window!"), TEXT("Error"), MB_OK);
    return nullptr;
}

void close_window(HWND window) {
    ShowWindow(window, 0);
    DestroyWindow(window);
}

void set_wheelfunctions(fn_wheel up, fn_wheel down) {
    wheel_up   = up;
    wheel_down = down;
}