#pragma once

#include <algorithm>
#include "types.h"

struct float4 {
    float x;
    float y;
    float z;
    float w;
};

struct float3 {
    float x;
    float y;
    float z;
};

struct float2 {
    float x;
    float y;
};

struct uint2 {
    U64 x;
    U64 y;
};

struct int2 {
    I64 x;
    I64 y;
};

inline float2 min(const float2& lhs, const float2& rhs) {
    return {
        std::min(lhs.x, rhs.x),
        std::min(lhs.y, rhs.y)
    };
}

inline float2 max(const float2& lhs, const float2& rhs) {
    return {
        std::max(lhs.x, rhs.x),
        std::max(lhs.y, rhs.y)
    };
}

inline float2 operator+(const float2& lhs, const float2& rhs) {
    return float2{
        lhs.x + rhs.x,
        lhs.y + rhs.y
    };
}

inline float2 operator-(const float2& lhs, const float2& rhs) {
    return float2{
        lhs.x - rhs.x,
        lhs.y - rhs.y
    };
}

inline float2 operator+=(float2& lhs, const float2& rhs) {
    return float2{
        lhs.x += rhs.x,
        lhs.y += rhs.y
    };
}

inline float2 operator-=(float2& lhs, const float2& rhs) {
    return float2{
        lhs.x -= rhs.x,
        lhs.y -= rhs.y
    };
}

inline float dot(const float2& lhs, const float2& rhs) {
    return (lhs.x*rhs.x + lhs.y*rhs.y);
}
inline float mag(const float2& v) {
    return sqrt(v.x*v.x + v.y*v.y);
}

inline float2 normalised(float2 v) {
    float div = mag(v);
    v.x /= div;
    v.y /= div;
    return v;
}

inline float2 scale(float factor, float2 op) {
    op.x *= factor;
    op.y *= factor;

    return op;
}

inline float3 operator+(const float3& lhs, const float3& rhs) {
    return float3{
        lhs.x + rhs.x,
        lhs.y + rhs.y,
        lhs.z + rhs.z
    };
}

inline float3 operator+=(float3& lhs, const float3& rhs) {
    return float3{
        lhs.x += rhs.x,
        lhs.y += rhs.y,
        lhs.z += rhs.z
    };
}

inline float3 operator-(const float3& lhs, const float3& rhs) {
    return float3{
        lhs.x - rhs.x,
        lhs.y - rhs.y,
        lhs.z - rhs.z
    };
}

inline float3 operator-(const float3& lhs) {
    return float3{
        -lhs.x,
        -lhs.y,
        -lhs.z
    };
}

inline float3 operator-=(float3& lhs, const float3& rhs) {
    return float3{
        lhs.x -= rhs.x,
        lhs.y -= rhs.y,
        lhs.z -= rhs.z
    };
}

inline float dot(const float3& lhs, const float3& rhs) {
    return (lhs.x*rhs.x + lhs.y*rhs.y + lhs.z*rhs.z);
}

inline float3 had(const float3& lhs, const float3& rhs) {
    return {
        lhs.x * rhs.x,
        lhs.y * rhs.y,
        lhs.z * rhs.z
    };
}

inline bool operator!=(const float3& lhs, const float3& rhs) {
    return (
        std::abs(lhs.x - rhs.x) > 0.0001 ||
        std::abs(lhs.y - rhs.y) > 0.0001 ||
        std::abs(lhs.z - rhs.z) > 0.0001);
}

inline float3 cross(const float3& lhs, const float3& rhs) {
    float3 rslt;
    rslt.x = lhs.y*rhs.z - lhs.z*rhs.y;
    rslt.y = lhs.z*rhs.x - lhs.x*rhs.z;
    rslt.z = lhs.x*rhs.y - lhs.y*rhs.x;

    return rslt;
}

inline float mag(const float3& v) {
    return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}

inline float3 normalised(float3 v) {
    float div = mag(v);
    v.x /= div;
    v.y /= div;
    v.z /= div;

    return v;
}

inline float3 scale(float factor, float3 op) {
    op.x *= factor;
    op.y *= factor;
    op.z *= factor;

    return op;
}

// lhs and rhs must be unit vectors
inline void orthogonalise(float3* lhs, float3* rhs) {
    float3 proj_rhs = scale(dot(*lhs, *rhs), *lhs);
    *rhs = normalised(*rhs - proj_rhs);
}

inline float3 min(const float3& lhs, const float3& rhs) {
    return {
        std::min(lhs.x, rhs.x),
        std::min(lhs.y, rhs.y),
        std::min(lhs.z, rhs.z)
    };
}

inline float3 max(const float3& lhs, const float3& rhs) {
    return {
        std::max(lhs.x, rhs.x),
        std::max(lhs.y, rhs.y),
        std::max(lhs.z, rhs.z)
    };
}
inline uint2 operator+(const uint2& lhs, const uint2& rhs) {
    return {
        lhs.x + rhs.x,
        lhs.y + rhs.y
    };
 }

inline bool operator==(const uint2& lhs, const uint2& rhs) {
    return {
        lhs.x == rhs.x &&
        lhs.y == rhs.y
    };
}

inline bool operator!=(const uint2& lhs, const uint2& rhs) {
    return {
        lhs.x != rhs.x ||
        lhs.y != rhs.y
    };
}

inline float4 scale(float factor, float4 op) {
    op.x *= factor;
    op.y *= factor;
    op.z *= factor;
    op.w *= factor;

    return op;
}

inline float4 operator+(const float4& lhs, const float4& rhs) {
    return float4{
        lhs.x + rhs.x,
        lhs.y + rhs.y,
        lhs.z + rhs.z,
        lhs.w + rhs.w
    };
}

inline float4 operator+=(float4& lhs, const float4& rhs) {
    return float4{
        lhs.x += rhs.x,
        lhs.y += rhs.y,
        lhs.z += rhs.z,
        lhs.w += rhs.w
    };
}

inline float4 operator-(const float4& lhs, const float4& rhs) {
    return float4{
        lhs.x - rhs.x,
        lhs.y - rhs.y,
        lhs.z - rhs.z,
        lhs.w - rhs.w
    };
}

inline float4 operator-(const float4& lhs) {
    return float4{
        -lhs.x,
        -lhs.y,
        -lhs.z,
        -lhs.w
    };
}

inline float4 operator-=(float4& lhs, const float4& rhs) {
    return float4{
        lhs.x -= rhs.x,
        lhs.y -= rhs.y,
        lhs.z -= rhs.z,
        lhs.w -= rhs.w
    };
}