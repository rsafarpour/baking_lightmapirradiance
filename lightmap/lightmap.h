#pragma once

#include "../vectortypes.h"
#include "../charts/chartmap.h"
#include <vector>

lightmap assemble_lightmap(const std::vector<chart_texture>* textures, chart_projection* projections, size_t lm_dims);