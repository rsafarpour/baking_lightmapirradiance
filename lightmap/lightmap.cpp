#include "lightmap.h"
#include "textures/textures.h"

uint2 vacant_position(const lightmap* lm, const chart_texture* chart_texture) {
    uint2 offset = {};
    for (U64 j = 0; j < chart_texture->dims.y; ++j) {
        // Need to move if one fragment's target position is occupied
        // or one fragment is out of bounds
        for (U64 i = 0; i < chart_texture->dims.x; ++i) {
            if ((load(chart_texture, uint2{ i,j }).w == 1.0 &&
                 load(lm, uint2{ i,j } + offset).w == 1.0f) ||
                offset.x + i > lm->dims.x ||
                offset.y + j > lm->dims.y) {
                offset.x++;
                offset.y = offset.y + offset.x/lm->dims.x;
                offset.x = offset.x % lm->dims.x;
                i = j = 0;
            }

            if (offset.y + chart_texture->dims.y >= lm->dims.y) {
                return {~0U, ~0U};
            }
        }
    }

    return offset;
}

void copy_chartmap(lightmap* lm, const chart_texture* texture, uint2 offset) {
    for (U64 j = 0; j < texture->dims.y; ++j) {
        for (U64 i = 0; i < texture->dims.x; ++i) {
            uint2 coord = uint2{ i,j };

            if (load(texture, coord).w > 0.0f) {
                store(lm, coord + offset, load(texture, coord));
            }
        }
    }
}

void convert_lmcoords(chart_projection* projections, std::vector<uint2>* offsets, lightmap* lm) {

    float texel_width = 1.0f / lm->dims.x;

    for (size_t i = 0; i < projections->rawlm_coords.size(); ++i) {
        chart_id cid = projections->charts[i];
        uint2 offset = (*offsets)[cid];

        float2 local_coords = projections->rawlm_coords[i];
        float2 global_coords = local_coords + float2{ float(offset.x), float(offset.y) };

        // Texel position to texture coordnates
        projections->rawlm_coords[i] = scale(texel_width, global_coords);
    }

}

lightmap assemble_lightmap(const std::vector<chart_texture>* textures, chart_projection* projections, size_t lm_dims) {
    lightmap lm;
    lm.dims = { lm_dims, lm_dims };
    lm.data.resize(4* lm_dims*lm_dims);


    std::vector<uint2> offsets(textures->size());
    for (size_t cid = 0; cid < textures->size(); ++cid) {
        uint2 locmap_offset = vacant_position(&lm, &(*textures)[cid]);
        if (locmap_offset != uint2{ ~0U, ~0U }) {
            offsets[cid] = locmap_offset;
            copy_chartmap(&lm, &(*textures)[cid], locmap_offset);
        }
    }

    convert_lmcoords(projections, &offsets, &lm);
    return lm;
}
