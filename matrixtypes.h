#pragma once

#include "vectortypes.h"

struct float3x3 {
    float _00; float _01; float _02;
    float _10; float _11; float _12;
    float _20; float _21; float _22;
};

struct float4x4 {
    float _00; float _01; float _02; float _03;
    float _10; float _11; float _12; float _13;
    float _20; float _21; float _22; float _23;
    float _30; float _31; float _32; float _33;
};


inline float3x3 construct(const float3* a, const float3* b, const float3* c) {
    return {
        a->x, b->x, c->x,
        a->y, b->y, c->y,
        a->z, b->z, c->z
    };
}

inline float3x3 transpose(const float3x3* m) {
    return {
        m->_00, m->_01, m->_02,
        m->_10, m->_11, m->_12,
        m->_20, m->_21, m->_22
    };
}

inline float3 mul(const float3x3* m, const float3* v) {
    return {
        m->_00*v->x + m->_01*v->y + m->_02*v->z,
        m->_10*v->x + m->_11*v->y + m->_12*v->z,
        m->_20*v->x + m->_21*v->y + m->_22*v->z
    };
}

inline float4x4 transposed(const float4x4* m) {
    return {
        m->_00, m->_10, m->_20, m->_30,
        m->_01, m->_11, m->_21, m->_31,
        m->_02, m->_12, m->_22, m->_32,
        m->_03, m->_13, m->_23, m->_33
    };
}

using rad_t = float;
using deg_t = float;
inline float4x4 ProjectionMatrixR(rad_t fov_y, float aspect_ratio, float n, float f) {
    return {
        1 / (tan(fov_y / 2)),                                 0.0,                     0.0,                   0.0,
        0.0,                  aspect_ratio * 1 / (tan(fov_y / 2)),                     0.0,                   0.0,
        0.0,                                                  0.0,      -(n + f) / (f - n),  -(2 * n*f) / (f - n),
        0.0,                                                  0.0,                    -1.0,                   0.0
    };
}

inline float4x4 ProjectionMatrixD(deg_t fov_y, float aspect_ratio, float n, float f) {
    return ProjectionMatrixR(rad_t{fov_y/180.0f * 3.14159f}, aspect_ratio, n, f);
}
