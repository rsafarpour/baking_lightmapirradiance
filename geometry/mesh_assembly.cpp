#include "mesh_assembly.h"
#include "../vectortypes.h"

vertex_desc custom_desc {
    {
        DXGI_FORMAT_R32G32B32_FLOAT,
        DXGI_FORMAT_R32G32B32_FLOAT,
        DXGI_FORMAT_R32G32B32_FLOAT,
        DXGI_FORMAT_R32G32B32_FLOAT,
        DXGI_FORMAT_R32G32_FLOAT
    },
    {
        "POSITION",
        "NORMAL",
        "ALBEDO",
        "EMITTANCE",
        "LM_COORDS"
    },
};

struct ray_format {
    float3 position;
};

vertex_desc rayfmt_desc {
    {DXGI_FORMAT_R32G32B32A32_FLOAT},
    { "POSITION" }
};

mesh_asm assemble_object(const static_geometry* geometry, const chart_projection* projections, size_t obj_id) {
    if (obj_id < geometry->objects.name.size()) {
        printf("Assembling object \"%s\"\n", geometry->objects.name[obj_id].c_str());

        mesh_asm assembly = {};
        assembly.vert_desc = custom_desc;
        assembly.topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

        size_t num_objverts = geometry->objects.num_objverts[obj_id];
        size_t num_objprims = geometry->objects.num_objprims[obj_id];
        size_t num_objinds = num_objprims*3;
        assembly.vertex_bytes = num_objverts* sizeof(custom_format);
        assembly.vertex_data  = malloc(assembly.vertex_bytes);
        assembly.index_bytes  = num_objinds*sizeof(index_t);
        assembly.index_data   = malloc(assembly.index_bytes);;

        size_t vert_base = geometry->objects.vert_base[obj_id];
        custom_format* vertices = (custom_format*)assembly.vertex_data;
        for (size_t i = 0; i < num_objverts; i < ++i) {
            vertices[i] = {};
            vertices[i].position  = geometry->verts.positions[vert_base + i];
            vertices[i].normal    = geometry->verts.normals[vert_base + i];
            vertices[i].albedo    = geometry->verts.albedo[vert_base + i];
            vertices[i].emittance = geometry->verts.emittance[vert_base + i];
            vertices[i].lm_coords = projections->rawlm_coords[vert_base + i];
        }

        size_t prim_base = geometry->objects.prim_base[obj_id];
        index_t* indices = (index_t*)assembly.index_data;
        for (size_t i = 0; i < num_objprims; i < ++i) {
            indices[(3*i) + 0] = geometry->prims.faces[prim_base + i].a - vert_base;
            indices[(3*i) + 1] = geometry->prims.faces[prim_base + i].b - vert_base;
            indices[(3*i) + 2] = geometry->prims.faces[prim_base + i].c - vert_base;
        }

        return assembly;
    }

    return {};
}

mesh_asm assemble_rays(float3 position, std::vector<float3>* dirs) {
    mesh_asm assembly = {};
    assembly.vert_desc = custom_desc;
    assembly.topology = D3D11_PRIMITIVE_TOPOLOGY_LINELIST;

    size_t num_verts = dirs->size() + 1;
    size_t num_prims = dirs->size();
    size_t num_inds = dirs->size() * 2;
    assembly.vertex_bytes = num_verts * sizeof(ray_format);
    assembly.vertex_data = malloc(assembly.vertex_bytes);
    assembly.index_bytes = num_inds * sizeof(index_t);
    assembly.index_data = malloc(assembly.index_bytes);;

    ray_format* vertices = (ray_format*)assembly.vertex_data;
    vertices[0].position = position;
    for (size_t i = 1; i < num_verts; i < ++i) {
        vertices[i].position = position + scale(5.0f, (*dirs)[i - 1]);
    }

    index_t* indices = (index_t*)assembly.index_data;
    for (size_t i = 0; i < num_prims; i < ++i) {
        indices[(2 * i) + 0] = 0;
        indices[(2 * i) + 1] = i + 1;
    }

    return assembly;
}