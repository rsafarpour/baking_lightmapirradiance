#pragma once

#define NOMINMAX
#include <d3d11.h>
#include <dxgi.h>
#include <vector>
#include "static_geometry.h"
#include "../charts/chartmap.h"

using topology_t = D3D11_PRIMITIVE_TOPOLOGY;

struct custom_format {
    position_t position;
    normal_t   normal;
    albedo_t   albedo;
    radiance_t emittance;
    texcoord_t lm_coords;
};

struct vertex_desc {
    std::vector<DXGI_FORMAT> data_formats;
    std::vector<const char*> semantics;
};

extern vertex_desc custom_desc;
extern vertex_desc rayfmt_desc;

struct mesh_asm {
    topology_t  topology;
    vertex_desc vert_desc;
    void*       vertex_data;
    size_t      vertex_bytes;
    void*       index_data;
    size_t      index_bytes;
};

mesh_asm assemble_object(const static_geometry* geometry, const chart_projection* projections, size_t obj_id);
mesh_asm assemble_rays(float3 position, std::vector<float3>* dirs);