#pragma once


#include "../types.h"
#include "geometry_types.h"
#include <vector>

using vert_id = U64;
using edge_id = U64;
using prim_id = U64;
using object_id = U64;

struct prim_indices {
    vert_id a;                             /* Index of vertex A of the primitive */
    vert_id b;                             /* Index of vertex B of the primitive */
    vert_id c;                             /* Index of vertex C of the primitive */
};

// Prim store is sorted by object_ids
struct prim_store {
    std::vector<object_id>    objects;     /* ID of the primitive's object         */
    std::vector<prim_indices> faces;       /* Vertices of a primitive              */
    size_t                    num_prims;   /* Number of primitives                 */
};

struct vertex_store {
    std::vector<position_t> positions;     /* Position of the vertex               */
    std::vector<radiance_t> emittance;     /* Position of the vertex               */
    std::vector<albedo_t>   albedo;        /* Albedo of the vertex                 */
    std::vector<normal_t>   normals;       /* Normal of the vertex                 */
};

struct object_store {
    std::vector<std::string> name;         /* Name of object                       */
    std::vector<vert_id>     prim_base;    /* Base ID of first vertex of object    */
    std::vector<vert_id>     vert_base;    /* Base ID of first primitive of object */
    std::vector<size_t>      num_objverts; /* Number of vertices in object         */
    std::vector<size_t>      num_objprims; /* Number of primitives in object       */
    size_t                   num_objects;  /* Number of objects in object store    */
};

struct static_geometry {
    object_store  objects;
    prim_store    prims;
    vertex_store  verts;
};

struct raycast_result_t {
    prim_id     prim;
    barycoord_t bary_intersection;
};