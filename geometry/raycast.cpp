#include "raycast.h"

inline barycoord_t intersection_barycentric(float3 ray_position, float3 ray_direction, float3 verts[3], const static_geometry* geometry) {
    float3 ab = verts[1] - verts[0];
    float3 ac = verts[2] - verts[0];
    float3 normal = normalised(cross(ab, ac));

    float d_dot_n = dot(ray_direction, normal);
    if (d_dot_n != 0) {
        float k = dot((verts[0] - ray_position), normal) / d_dot_n;
        if (k > 0.0f) {
            float3 p = ray_position + scale(k, ray_direction);

            float3 v0 = verts[1] - verts[0], v1 = verts[2] - verts[0], v2 = p - verts[0];
            float d00 = dot(v0, v0);
            float d01 = dot(v0, v1);
            float d11 = dot(v1, v1);
            float d20 = dot(v2, v0);
            float d21 = dot(v2, v1);
            float denom = d00 * d11 - d01 * d01;
            float v = (d11 * d20 - d01 * d21) / denom;
            float w = (d00 * d21 - d01 * d20) / denom;
            float u = 1.0f - v - w;

            if (v >= 0.0f && v <= 1.0 &&
                w >= 0.0f && w <= 1.0 &&
                u >= 0.0f && u <= 1.0) {
                return { u, v, w };
            }
        }
    }

    return barycoord_invalid;
}

std::vector<raycast_result_t> raycast(float3 position, std::vector<float3>* directions, const static_geometry* geometry) {
    std::vector<raycast_result_t> results;
    std::vector<float3> new_directions;
    new_directions.reserve(directions->size());
    for (const float3& dir : *directions) {
        float min_distance = FLT_MAX;
        raycast_result_t closest = { 0,barycoord_invalid };
        for (size_t i = 0; i < geometry->prims.faces.size(); ++i) {
            float3 a = geometry->verts.positions[geometry->prims.faces[i].a];
            float3 b = geometry->verts.positions[geometry->prims.faces[i].b];
            float3 c = geometry->verts.positions[geometry->prims.faces[i].c];
            float3 verts[] = { a,b,c };
            barycoord_t bary_intersection = intersection_barycentric(position, dir, verts, geometry);
            float3  intersection = scale(bary_intersection.x, a)
                + scale(bary_intersection.y, b)
                + scale(bary_intersection.z, c);
            float distance = mag(position - intersection);

            if (bary_intersection != barycoord_invalid &&
                distance < min_distance &&
                distance > 0.0001) {
                min_distance = distance;
                closest = { i, bary_intersection };
            }
        }

        normal_t n_a = geometry->verts.normals[geometry->prims.faces[closest.prim].a];
        normal_t n_b = geometry->verts.normals[geometry->prims.faces[closest.prim].b];
        normal_t n_c = geometry->verts.normals[geometry->prims.faces[closest.prim].c];
        normal_t normal = scale(closest.bary_intersection.x, n_a)
            + scale(closest.bary_intersection.y, n_b)
            + scale(closest.bary_intersection.z, n_c);
        if (closest.bary_intersection != barycoord_invalid &&
            dot(normal, dir) < 0.0f) {
            results.push_back(closest);
            new_directions.push_back(dir);
        }
    }

    *directions = std::move(new_directions);
    return results;
}

std::vector<raycast_result_t> raycast(float3 position, std::vector<float3>* directions, const static_geometry* geometry, const BVH* bvh) 
{
    std::vector<raycast_result_t> results;
    std::vector<float3> new_directions;
    new_directions.reserve(directions->size());
    for (const float3& dir : *directions) {
        float min_distance = FLT_MAX;
        raycast_result_t closest = { 0,barycoord_invalid };
        std::vector<prim_id> candidates = find_candidates(bvh, position, dir);
        for (auto candidate : candidates) {
            float3 a = geometry->verts.positions[geometry->prims.faces[candidate].a];
            float3 b = geometry->verts.positions[geometry->prims.faces[candidate].b];
            float3 c = geometry->verts.positions[geometry->prims.faces[candidate].c];
            float3 verts[] = { a,b,c };
            barycoord_t bary_intersection = intersection_barycentric(position, dir, verts, geometry);
            float3  intersection = scale(bary_intersection.x, a)
                + scale(bary_intersection.y, b)
                + scale(bary_intersection.z, c);
            float distance = mag(position - intersection);

            if (bary_intersection != barycoord_invalid &&
                distance < min_distance &&
                distance > 0.0001) {
                min_distance = distance;
                closest = { candidate, bary_intersection };
            }
        }

        normal_t n_a = geometry->verts.normals[geometry->prims.faces[closest.prim].a];
        normal_t n_b = geometry->verts.normals[geometry->prims.faces[closest.prim].b];
        normal_t n_c = geometry->verts.normals[geometry->prims.faces[closest.prim].c];
        normal_t normal = scale(closest.bary_intersection.x, n_a)
            + scale(closest.bary_intersection.y, n_b)
            + scale(closest.bary_intersection.z, n_c);
        if (closest.bary_intersection != barycoord_invalid &&
            dot(normal, dir) < 0.0f) {
            results.push_back(closest);
            new_directions.push_back(dir);
        }
    }

    *directions = std::move(new_directions);
    return results;
}