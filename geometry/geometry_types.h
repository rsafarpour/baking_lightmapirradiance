#pragma once

#include "../vectortypes.h"

using normal_t    = float3;
using position_t  = float3;
using radiance_t  = float3;
using albedo_t    = float3;
using texcoord_t  = float2;
using barycoord_t = float3;
using index_t     = U32;


constexpr barycoord_t barycoord_invalid = { 2.0f, 2.0f, 2.0f };

inline bool valid(const barycoord_t& coords) { return coords.x <= 1.0f; }
