#pragma once
#include "static_geometry.h"
#include "bvh.h"

std::vector<raycast_result_t> raycast(float3 position, std::vector<float3>* directions, const static_geometry* geometry);
std::vector<raycast_result_t> raycast(float3 position, std::vector<float3>* directions, const static_geometry* geometry, const BVH* bvh);