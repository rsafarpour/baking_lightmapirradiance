#pragma once
#include "static_geometry.h"
#include "../vectortypes.h"

using morton_id = U32;

struct aabb_t
{
    float3 min;
    float3 max;
};

struct BVH {
    std::vector<aabb_t>  bounds;
    std::vector<size_t>  volume_firstprim;
    std::vector<size_t>  volume_numprims;
    std::vector<size_t>  skip_vals;
    std::vector<prim_id> prim_ids;
};

aabb_t               union_aabb(const std::vector<prim_id>* prim_ids, const std::vector<aabb_t>* aabbs, size_t index_begin, size_t index_end);
BVH                  construct(const static_geometry* geometry);
std::vector<prim_id> find_candidates(const BVH* bvh, float3 position, float3 dir);