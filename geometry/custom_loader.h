#pragma once

#include "../types.h"
#include "../vectortypes.h"
#include "geometry_types.h"

struct custom_fileformat {
    position_t position;
    albedo_t   albedo;
    radiance_t radiance;
    normal_t   normal;
    texcoord_t texcoords;
};

struct static_geometry;
class custom_loader
{
public:
    void Load(static_geometry* geometry, const char* filename);
};


