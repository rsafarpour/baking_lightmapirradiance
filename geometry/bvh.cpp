#include "../sorting.h"
#include "bvh.h"
#include <cassert>
#include <numeric>
#include <stack>

#define bitsizeof(type) (sizeof(type)*8)

#define insert_offset(container, offset, val) container.insert(container.begin() + offset, val)

using morton_iter = std::vector<morton_id>::iterator;
struct morton_range {
    morton_iter begin;
    morton_iter end;
    morton_id splitval;
};

U32 morton3D(const float3* pos)
{
    // Based on https://devblogs.nvidia.com/thinking-parallel-part-iii-tree-construction-gpu/
    U32 x = (U32)pos->x;
    x = (x * 0x00010001u) & 0xFF0000FFu;
    x = (x * 0x00000101u) & 0x0F00F00Fu;
    x = (x * 0x00000011u) & 0xC30C30C3u;
    x = (x * 0x00000005u) & 0x49249249u;

    U32 y = (U32)pos->y;
    y = (y * 0x00010001u) & 0xFF0000FFu;
    y = (y * 0x00000101u) & 0x0F00F00Fu;
    y = (y * 0x00000011u) & 0xC30C30C3u;
    y = (y * 0x00000005u) & 0x49249249u;

    U32 z = (U32)pos->z;
    z = (z * 0x00010001u) & 0xFF0000FFu;
    z = (z * 0x00000101u) & 0x0F00F00Fu;
    z = (z * 0x00000011u) & 0xC30C30C3u;
    z = (z * 0x00000005u) & 0x49249249u;
    
    U32 xx = (x << 2);
    U32 yy = (y << 1);
    U32 zz = z;
    U32 code = (xx | yy | zz);
    return code;
}

std::vector<morton_id> calc_mortoncodes(const std::vector<aabb_t>* aabbs) {
    std::vector<float3> centroids(aabbs->size());
    float3 root_min = {  FLT_MAX,  FLT_MAX,  FLT_MAX };
    float3 root_max = { -FLT_MAX, -FLT_MAX, -FLT_MAX };
    for (size_t i = 0; i < aabbs->size(); ++i) {
        float3 centroid = scale(0.5, (*aabbs)[i].max - (*aabbs)[i].min);

        root_min = min(root_min, centroid);
        root_max = max(root_max, centroid);
        centroids[i] = centroid;
    }

    std::vector<morton_id> morton_codes(centroids.size());
    for (U32 i = 0; i < centroids.size(); ++i) {
        float3 normalized = had(centroids[i], root_max - root_min);
        normalized = min(max({ 0.0f, 0.0f, 0.0f }, scale(1024.0, normalized)), { 1023.0f, 1023.0f, 1023.0f });
        morton_codes[i] = morton3D(&normalized);
    }

    return morton_codes;
}

std::vector<aabb_t> calc_aabbs(const std::vector<prim_indices>* faces, const std::vector<position_t>* vert_pos) {
    std::vector<aabb_t> aabbs(faces->size());

    for (size_t i = 0; i < faces->size(); ++i) {
        float3 pos_a = vert_pos->operator[](faces->operator[](i).a);
        float3 pos_b = vert_pos->operator[](faces->operator[](i).b);
        float3 pos_c = vert_pos->operator[](faces->operator[](i).c);
        float3 aabb_min = min(min(pos_a, pos_b), pos_c);
        float3 aabb_max = max(max(pos_a, pos_b), pos_c);
        float3 centroid = scale(0.5, aabb_max - aabb_min);

        aabbs[i].min = aabb_min;
        aabbs[i].max = aabb_max;
    }

    return aabbs;
}

void calc_vol(morton_iter base, const std::vector<morton_range>* ranges, const std::vector<aabb_t>* prim_aabbs, const std::vector<prim_id>* prim_ids, std::vector<aabb_t>* volume_aabbs) {
    volume_aabbs->resize(ranges->size());
    for (auto range_id = 0; range_id != ranges->size(); ++range_id) {
        morton_range range = (*ranges)[range_id];
        size_t i_begin = range.begin - base;
        size_t i_end = range.end - base;

        (*volume_aabbs)[range_id] = union_aabb(prim_ids, prim_aabbs, i_begin, i_end);
    }
}


// Lambda returns true if bit at bit_pos(counting zero based from MSB) is set
auto is_split = [](I8 bit_pos, morton_id id) {
    morton_id test_bit = 1 << (bitsizeof(morton_id) - bit_pos - 1);
    return ((test_bit & id) > 0);
};

void build_hierarchy(std::vector<morton_id>* morton_codes, const std::vector<aabb_t>* volumes, BVH* bvh) {
    assert(morton_codes->size() > 0);
    // This function generates a list of iterator ranges so that
    // each volume range is followed by the ranges of its subvolumes, i.e.:
    // range_vol0 -> range_vol0subvol0 -> range_vol0subvol1 -> range_vol1 -> ...
    // Lookup structure generation is described here: 
    // https://devblogs.nvidia.com/thinking-parallel-part-iii-tree-construction-gpu/
    bvh->skip_vals.push_back(0);
    std::vector<size_t> counting_subvols;

    std::vector<morton_range> volume_ranges;
    volume_ranges.push_back({morton_codes->begin(), morton_codes->end(), 0});
    bvh->volume_firstprim.push_back(0);
    bvh->volume_numprims.push_back(morton_codes->size());
    for (size_t range_id = 0; range_id != volume_ranges.size(); range_id++) {
        morton_range range = volume_ranges[range_id];
        morton_id test_bit = range.splitval + 1;
        auto split = std::upper_bound(range.begin, range.end, test_bit, is_split);
        while ((split == range.begin || split == range.end) &&
            test_bit < bitsizeof(morton_id)) {
            test_bit += 1;
            split = std::upper_bound(range.begin, range.end, test_bit, is_split);
        }

        if (split != range.begin && split != range.end) {
            counting_subvols.push_back(range_id);
            for (size_t subvol : counting_subvols) {
                bvh->skip_vals[subvol] += 2;
            }

            size_t index_l = range.begin - morton_codes->begin();
            size_t index_c = split - morton_codes->begin();
            size_t index_r = range.end - morton_codes->begin();;
            insert_offset(bvh->volume_firstprim, range_id + 1, index_c);
            insert_offset(bvh->volume_firstprim, range_id + 1, index_l);
            insert_offset(bvh->volume_numprims, range_id + 1, index_r - index_c);
            insert_offset(bvh->volume_numprims, range_id + 1, index_c - index_l);

            morton_range range_r = { split, range.end, test_bit };
            morton_range range_l = { range.begin, split, test_bit };
            volume_ranges.insert(volume_ranges.begin() + range_id + 1, range_r);
            volume_ranges.insert(volume_ranges.begin() + range_id + 1, range_l);
            bvh->skip_vals.insert(bvh->skip_vals.begin() + range_id + 1, 0);
            bvh->skip_vals.insert(bvh->skip_vals.begin() + range_id + 1, 0);
        }

        // True every time a parent subvolume has been completed -> stop
        // counting new subvolumes for that parent
        if (split == volume_ranges[counting_subvols.back()].end) {
            counting_subvols.pop_back();
        }
    }

    calc_vol(morton_codes->begin(), &volume_ranges, volumes, &bvh->prim_ids, &bvh->bounds);
}

aabb_t union_aabb(const std::vector<prim_id>* prim_ids, const std::vector<aabb_t>* aabbs, size_t index_begin, size_t index_end) {


    aabb_t accum = { 
        { FLT_MAX,  FLT_MAX,  FLT_MAX},
        {-FLT_MAX, -FLT_MAX, -FLT_MAX}
    };

    for (; index_begin < index_end; ++index_begin) {
        accum.min = min(accum.min, (*aabbs)[index_begin].min);
        accum.max = max(accum.max, (*aabbs)[index_begin].max);
    }

    return accum;
}

BVH construct(const static_geometry* geometry) {
    std::vector<aabb_t> aabbs = calc_aabbs(&geometry->prims.faces, &geometry->verts.positions);
    std::vector<morton_id> morton_codes = calc_mortoncodes(&aabbs);

    BVH bvh;
    bvh.prim_ids.resize(geometry->prims.faces.size());
    std::iota(bvh.prim_ids.begin(), bvh.prim_ids.end(), 0);
    sort_key k = record_sorting(&morton_codes);
    morton_codes = get_sorted(&k, &morton_codes);
    bvh.prim_ids = get_sorted(&k, &bvh.prim_ids);
    aabbs = get_sorted(&k, &aabbs);

    build_hierarchy(&morton_codes, &aabbs, &bvh);

    return bvh;
}

bool inside(float3 position, aabb_t bounds) {
    return (position.x - bounds.min.x > -0.001f &&
            position.y - bounds.min.y > -0.001f &&
            position.z - bounds.min.z > -0.001f &&
            position.x - bounds.max.x <  0.001f &&
            position.y - bounds.max.y <  0.001f &&
            position.z - bounds.max.z <  0.001f);
}

bool intersects(float3 position, float3 dir, aabb_t bounds) {
    // Find closest plane
    normal_t normals[] = {
        {1,0,0}, {0,1,0}, {0,0,1},
        {-1,0,0}, {0,-1,0}, {0,0,-1},
    };

    position_t plane_p[] = { bounds.max, bounds.min };

    size_t closest = 0;
    float kmin = FLT_MAX;
    for (size_t i = 0; i < 6; ++i) {
        float k = dot(normals[i], plane_p[i/3] - position) / dot(dir, normals[i]);
        
        if (k > 0) {
            float3 plane_projection = position + scale(kmin, dir);
            if (inside(plane_projection, bounds)) { return true; }
        }
    }
}

std::vector<prim_id> find_candidates(const BVH* bvh, float3 position, float3 dir) {
    std::vector<prim_id> candidates;
    size_t num_subvols = bvh->skip_vals.size();
    size_t subvol = 0;
    while (subvol < num_subvols) {
        if (!(inside(position, bvh->bounds[subvol]) ||
            intersects(position, dir, bvh->bounds[subvol]))) {
            subvol += bvh->skip_vals[subvol];
        }
        else if (bvh->skip_vals[subvol] == 0) { // This indicates that volume is lowest level
            size_t begin = bvh->volume_firstprim[subvol];
            size_t end = begin + bvh->volume_numprims[subvol];
            for (size_t i = begin; i < end; ++i) {
                candidates.push_back(bvh->prim_ids[i]);
            }

        }
        subvol++;
    }
    

    return candidates;
}