#include "custom_loader.h"
#include "static_geometry.h"

#include "../files/file.h"

#define NUM_VERTEXFLOATS 12
#define NUM_TRIVERTICES   3

#define MAX_IDCHAR 30

#define IS_DIGIT(d) (d <= 0x39 && d >= 0x30)
#define DIGIT_TO_INT(d) (d - '0')

static void read_id(std::string* content, U64* file_pos, unsigned char* id, U32 max_length)
{
    max_length -= 1;

    U32 index = 0;
    unsigned char c = (*content)[(*file_pos)++];;
    while (c != '\n' && index < max_length)
    {
        id[index++] = c;
        c = (*content)[(*file_pos)++];
    }

    id[index] = '\0';

    // Scan past end of id without saving it
    while (c != '\n')
    {
        c = (*content)[(*file_pos)++];
    }
}

static bool read_integer(std::string* content, U64* file_pos, U64* num)
{
    unsigned char digit = (*content)[*file_pos];
    (*file_pos)++;

    *num = 0;
    while (digit != '\n')
    {
        if (IS_DIGIT(digit))
        {
            *num *= 10;
            *num += DIGIT_TO_INT(digit);
        }
        else
        {
            return false;
        }
        digit = (*content)[*file_pos];
        (*file_pos)++;
    }
    return true;
}

static void read_float(std::string* content, U64* file_pos, float* num)
{
    // Vertex format is fixed to 3x position, 3x normal, 4x rgba color
    U8 index = 0;
    U64 substr_len = 0;
    unsigned char c = (*content)[*file_pos + substr_len];
    while (c != ' ' && c != '\n' && c != EOF) {
        c = (*content)[*file_pos + substr_len++];
    }

    *num = strtof(content->substr(*file_pos, substr_len - 1).data(), nullptr);
    *file_pos += substr_len;
}

static void skip_whitespace(std::string* content, U64* file_pos) {
    while (*file_pos < content->size() && 
        ((*content)[*file_pos] == ' ' || (*content)[*file_pos] == '\n')) {
        ++(*file_pos);
    }
}

static void read_vertex(std::string* content, U64* file_pos, custom_fileformat* vertex)
{
    // Custom vertex format is fixed to 3x position, 3x normal, 3x albedo 3x radiance
    read_float(content, file_pos, &vertex->position.x); skip_whitespace(content, file_pos);
    read_float(content, file_pos, &vertex->position.y); skip_whitespace(content, file_pos);
    read_float(content, file_pos, &vertex->position.z); skip_whitespace(content, file_pos);

    read_float(content, file_pos, &vertex->normal.x); skip_whitespace(content, file_pos);
    read_float(content, file_pos, &vertex->normal.y); skip_whitespace(content, file_pos);
    read_float(content, file_pos, &vertex->normal.z); skip_whitespace(content, file_pos);

    read_float(content, file_pos, &vertex->albedo.x); skip_whitespace(content, file_pos);
    read_float(content, file_pos, &vertex->albedo.y); skip_whitespace(content, file_pos);
    read_float(content, file_pos, &vertex->albedo.z); skip_whitespace(content, file_pos);

    read_float(content, file_pos, &vertex->radiance.x); skip_whitespace(content, file_pos);
    read_float(content, file_pos, &vertex->radiance.y); skip_whitespace(content, file_pos);
    read_float(content, file_pos, &vertex->radiance.z); skip_whitespace(content, file_pos);

}

static void read_tri(std::string* content, U64* file_pos, prim_indices* indices)
{
    read_integer(content, file_pos, &indices->a);
    read_integer(content, file_pos, &indices->b);
    read_integer(content, file_pos, &indices->c);
}

void custom_loader::Load(static_geometry* geometry, const char* filename) {
    U64 filesize = filesize_a(filename);
 
    std::string content;
    content.resize(filesize + 1);
    U64 content_size = read_a(filename, &content, filesize);
    U64 index_offset = 0;
    if (content_size > 0) {
        U64 num_vertices = 0;
        U64 num_indices  = 0;
        U64 file_pos     = 0;
        if (read_integer(&content, &file_pos, &num_vertices) && read_integer(&content, &file_pos, &num_indices))
        {
            unsigned char object_id[MAX_IDCHAR];
            unsigned char line_id = content[file_pos++];
            U32 vert_idx = 0;
            while (file_pos < content_size)
            {
                file_pos++; // Skip mandatory space
                switch (line_id) {
                case 'o':
                    read_id(&content, &file_pos, object_id, MAX_IDCHAR);
                    geometry->objects.name.emplace_back((char*)object_id);
                    
                    geometry->objects.vert_base.push_back(geometry->verts.positions.size());
                    geometry->objects.prim_base.push_back(geometry->prims.faces.size());
                    geometry->objects.num_objverts.push_back(0);
                    geometry->objects.num_objprims.push_back(0);
                    geometry->objects.num_objects++;
                    index_offset = vert_idx;
                    break;
                case 'v':
                    custom_fileformat vertex;
                    read_vertex(&content, &file_pos, &vertex);
                    geometry->verts.emittance.push_back(vertex.radiance);
                    geometry->verts.albedo.push_back(vertex.albedo);
                    geometry->verts.normals.push_back(vertex.normal);
                    geometry->verts.positions.push_back(vertex.position);
                    geometry->objects.num_objverts.back()++;
                    vert_idx++;
                    break;
                case 'f':
                    prim_indices indices;
                    read_tri(&content, &file_pos, &indices);
                    indices.a += index_offset;
                    indices.b += index_offset;
                    indices.c += index_offset;
                    geometry->prims.faces.push_back(indices);

                    geometry->objects.num_objprims.back()++;
                    geometry->prims.num_prims++;
                    break;
                default:
                    return;
                }

                if (file_pos < content.size()) {
                    skip_whitespace(&content, &file_pos);
                    line_id = content[file_pos++];
                }
            }
        }
    }
}