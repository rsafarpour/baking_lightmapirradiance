#pragma once

#include "charts/chart_types.h"
#include "texture_types.h"
#include <vector>

std::vector<chart_texture> create_textures(const std::vector<chart_fragments>* chart_fragments);
void                       store(chart_texture* texture, uint2 location, float4 value);
void                       store(lightmap* texture, uint2 location, float4 value);
float4                     load(const chart_texture* texture, uint2 location);
float4                     load(const lightmap* lm, uint2 location);
float4                     sample(const chart_texture* texture, float2 uv);
void                       debug_out(const std::vector<chart_texture>* textures);
void                       debug_out(const lightmap* textures);
