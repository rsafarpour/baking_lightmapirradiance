#include "textures.h"
#include "bitmap/bitmap.h"
#include <string>

std::vector<chart_texture> create_textures(const std::vector<chart_fragments>* chart_fragments) {
    std::vector<chart_texture> textures;
    for (auto& cf : *chart_fragments) {
        chart_texture texture;
        texture.dims = cf.dims;
        texture.data.resize(4*texture.dims.x*texture.dims.y);

        for (auto& f : cf.fragments) {
            store(&texture, f.coord, {0.0, 0.0, 0.0, 1.0});
        }
        textures.push_back(texture);
    }

    return textures;
}

void store(chart_texture* texture, uint2 location, float4 value) {
    texture->data[4*(location.x + location.y*texture->dims.x) + 0] = value.x;
    texture->data[4*(location.x + location.y*texture->dims.x) + 1] = value.y;
    texture->data[4*(location.x + location.y*texture->dims.x) + 2] = value.z;
    texture->data[4*(location.x + location.y*texture->dims.x) + 3] = value.w;
}

void store(lightmap* texture, uint2 location, float4 value) {
    texture->data[4 * (location.x + location.y*texture->dims.x) + 0] = value.x;
    texture->data[4 * (location.x + location.y*texture->dims.x) + 1] = value.y;
    texture->data[4 * (location.x + location.y*texture->dims.x) + 2] = value.z;
    texture->data[4 * (location.x + location.y*texture->dims.x) + 3] = value.w;
}

float4 load(const chart_texture* texture, uint2 location) {
    return {
        texture->data[4 * (location.x + location.y*texture->dims.x) + 0],
        texture->data[4 * (location.x + location.y*texture->dims.x) + 1],
        texture->data[4 * (location.x + location.y*texture->dims.x) + 2],
        texture->data[4 * (location.x + location.y*texture->dims.x) + 3]
    };
}

float4 load(const lightmap* lm, uint2 location) {
    return {
        lm->data[4 * (location.x + location.y*lm->dims.x) + 0],
        lm->data[4 * (location.x + location.y*lm->dims.x) + 1],
        lm->data[4 * (location.x + location.y*lm->dims.x) + 2],
        lm->data[4 * (location.x + location.y*lm->dims.x) + 3]
    };
}

float4 sample(const chart_texture* texture, float2 uv) {
    float2 access0 = uv + float2{ -0.5f, -0.5f };  
    float2 access1 = uv + float2{  0.5f, -0.5f };  
    float2 access2 = uv + float2{ -0.5f,  0.5f };  
    float2 access3 = uv + float2{  0.5f,  0.5f };  
    access0.x = std::min(access0.x, float(texture->dims.x)); access0.y = std::min(access0.y, float(texture->dims.y));
    access1.x = std::min(access1.x, float(texture->dims.x)); access1.y = std::min(access1.y, float(texture->dims.y));
    access2.x = std::min(access2.x, float(texture->dims.x)); access2.y = std::min(access2.y, float(texture->dims.y));
    access3.x = std::min(access3.x, float(texture->dims.x)); access3.y = std::min(access3.y, float(texture->dims.y));
    size_t aindex0 = 3 * (size_t(access0.x) + size_t(access0.y)*texture->dims.x);
    size_t aindex1 = 3 * (size_t(access1.x) + size_t(access1.y)*texture->dims.x);
    size_t aindex2 = 3 * (size_t(access2.x) + size_t(access2.y)*texture->dims.x);
    size_t aindex3 = 3 * (size_t(access3.x) + size_t(access3.y)*texture->dims.x);
    float rweight = floor(access3.x) + 0.5f - uv.x;
    float tweight = floor(access3.y) + 0.5f - uv.y;
    float lweight = uv.x - floor(access0.x) - 0.5f;
    float bweight = uv.y - floor(access0.y) - 0.5f;
    float4 tl = { texture->data[aindex0 + 0], texture->data[aindex0 + 1], texture->data[aindex0 + 2], texture->data[aindex0 + 3] };
    float4 tr = { texture->data[aindex1 + 0], texture->data[aindex1 + 1], texture->data[aindex1 + 2], texture->data[aindex1 + 3] };
    float4 bl = { texture->data[aindex2 + 0], texture->data[aindex2 + 1], texture->data[aindex2 + 2], texture->data[aindex2 + 3] };
    float4 br = { texture->data[aindex3 + 0], texture->data[aindex3 + 1], texture->data[aindex3 + 2], texture->data[aindex3 + 3] };

    // Bilinearily interpolate
    return {
        scale(tweight, scale(lweight, tl) + scale(rweight, tr)) +
        scale(bweight, scale(lweight, bl) + scale(rweight, br))
    };
}

void debug_out(const std::vector<chart_texture>* textures) {
    size_t id = 0;
    for (const chart_texture& t : *textures) {
        std::string fname = "__debug/lightmap_" + std::to_string(id) + ".bmp";
        write_bmp(fname.c_str(), t.data.data(), 40.0, 0.0, t.dims.x, t.dims.y);
        id++;
    }
}

void debug_out(const lightmap* lm) {
    std::string fname = "__debug/final_lightmap.bmp";
    write_bmp(fname.c_str(), lm->data.data(), 40.0, 0.0, lm->dims.x, lm->dims.y);
}
