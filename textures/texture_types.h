#pragma once

#include "../vectortypes.h"
#include <vector>

struct chart_texture {
    std::vector<float> data;
    uint2              dims;
};

struct lightmap {
    std::vector<float> data;
    uint2              dims;
};