#pragma once
#include "../vectortypes.h"
#include <vector>

std::vector<float3> hemispherical_directions(const float3* up, size_t num);