#include "sample.h"
#include "../matrixtypes.h"
#include "../vectortypes.h"

#include <random>

float2 polar_to_cartesian(float r, float theta) {
    return {
        r * cos(theta),
        r * sin(theta)
    };
};

float3 any_orthogonal_of(const float3* v) {
    // Find a vector that is definitely different 
    // from a (unless it is (0,0,0)
    float3 t1 = { v->y, v->x, v->z };
    float3 t2 = { v->x, v->z, v->y };
    return normalised(cross(*v, t1+t2));
}

std::vector<float3> hemispherical_directions(const float3* up, size_t num) {
    // Change of basis function for current vertex
    float3 right = any_orthogonal_of(up);
    float3 forward = normalised(cross(*up, right));
    float3x3 tnb = construct(&right, up, &forward);
    transpose(&tnb);

    constexpr float pi = 3.14159265359f;
    std::random_device rd;
    std::uniform_real_distribution<float> dist_r(0, 1.0);
    std::uniform_real_distribution<float> dist_theta(0, 2 * pi);

    std::vector<float3> dirs;
    for (size_t i = 0; i < num; i++)
    {
        float r = sqrt(dist_r(rd));
        float theta = 2 * pi*dist_theta(rd);

        float2 coord_disk = polar_to_cartesian(r, theta);
        float x = coord_disk.x;
        float z = coord_disk.y;
        float y = sqrt(1 - x * x - z * z);

        float3 ts_dir = normalised({ x, y, z }); // tangent space
        float3 ws_dir = mul(&tnb, &ts_dir);      // world space

        dirs.push_back(ws_dir);
    }

    return dirs;
}