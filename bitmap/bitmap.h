#pragma once

#include "types.h"
#include "../files/file.h"

#pragma pack(push,1)
struct bitmapfileheader_t
{
    char bfType[2] = {'B', 'M'};
    U32  bfSize;
    U32  bfReserved;
    U32  bfOffBits;
};
#pragma pack(pop)

#pragma pack(push,1)
struct bitmapinfoheader_t
{
    U32 biSize          = sizeof(bitmapinfoheader_t);
    U32 biWidth;
    U32 biHeight;
    U16 biPlanes        = 1;
    U16 biBitCount      = 24;
    U32 biCompression   = 0;
    U32 biSizeImage;
    U32 biXPelsPerMeter = 2835;
    U32 biYPelsPerMeter = 2835;
    U32 biClrUsed       = 0;
    U32 biClrImportant  = 0;
};
#pragma pack(pop)

inline void write_bmp(const char* filename, const float* image, float scale, float bias, U32 width, U32 height) {
    U32 rowdata_bytes = (width * sizeof(U8)*3 + 3) & 0xFFFFFFFC;
    U32 imgdata_bytes = height * rowdata_bytes;

    std::string file_content;
    file_content.resize(sizeof(bitmapfileheader_t) + sizeof(bitmapinfoheader_t) + imgdata_bytes);
    bitmapfileheader_t* file_header = (bitmapfileheader_t*) file_content.data();
    bitmapinfoheader_t* info_header = (bitmapinfoheader_t*)(file_content.data() + sizeof(bitmapfileheader_t));
    U8* img_data = (U8*)(file_content.data() + sizeof(bitmapfileheader_t) + sizeof(bitmapinfoheader_t));

    file_header->bfType[0] = 'B';
    file_header->bfType[1] = 'M';
    file_header->bfSize = sizeof(bitmapfileheader_t) + sizeof(bitmapinfoheader_t) + imgdata_bytes;
    file_header->bfReserved = 0;
    file_header->bfOffBits = sizeof(bitmapfileheader_t) + sizeof(bitmapinfoheader_t);

    info_header->biSize          = sizeof(bitmapinfoheader_t);
    info_header->biWidth         = width;
    info_header->biHeight        = height;
    info_header->biPlanes        = 1;
    info_header->biBitCount      = 24;
    info_header->biCompression   = 0;
    info_header->biSizeImage     = imgdata_bytes;
    info_header->biXPelsPerMeter = 2835;
    info_header->biYPelsPerMeter = 2835;
    info_header->biClrUsed       = 0;
    info_header->biClrImportant  = 0;

    U32 img_index = 0;
    for (U32 i = 0; i < height; ++i)
    {
        U32 row_idx = (height - i - 1) * rowdata_bytes; // works bc img_data == U8[];
        for (U32 j = 0; j < width; ++j)
        {
            img_data[row_idx + (j * 3) + 0] = U8(std::min(255.0f, bias + scale*image[img_index*4 + 2]));
            img_data[row_idx + (j * 3) + 1] = U8(std::min(255.0f, bias + scale*image[img_index*4 + 1]));
            img_data[row_idx + (j * 3) + 2] = U8(std::min(255.0f, bias + scale*image[img_index*4 + 0]));
            ++img_index;
        }
    }

    write_b(filename, file_content.data(), file_header->bfSize);
}
