#define NOMINMAX

#include <windows.h>
#include <windowsx.h>

#include "geometry/bvh.h"
#include "geometry/custom_loader.h"
#include "geometry/static_geometry.h"
#include "charts/chart_collection.h"
#include "charts/chartmap.h"
#include "rasterising/rasterise.h"
#include "bitmap/bitmap.h"
#include "sampling/sample.h"
#include "shell/window.h"
#include "rendering/dx11render.h"
#include "rendering/gpu_compiler.h"
#include "lightmap/lightmap.h"
#include "matrixtypes.h"

float4x4 view = {};
float4x4 proj = ProjectionMatrixD(90.0f, 4.0f / 3.0f, 1.0f, 200.0f);

void mousewheel_up() {
    view._23 += 0.1f;
    view._23 = std::min(view._23, -2.0f);
}

void mousewheel_down() {
    view._23 -= 0.1f;
    view._23 = std::max(view._23, -20.0f);
}

ref prepare_pipeline(dx11renderer* renderer) {
    
    // Setup view and projection matrix
    proj = ProjectionMatrixD(90.0f, 4.0f / 3.0f, 1.0f, 200.0f);
    view = {
        1.0, 0.0, 0.0,  0.0,
        0.0, 1.0, 0.0, -2.0,
        0.0, 0.0, 1.0, -2.0,
        0.0, 0.0, 0.0,  1.0,
    };

    // Prepare pipeline state
    gpu_bytecode bytecode = compile_from_file(custom_desc, "shaders/object.vs", "shaders/object.ps", SAMPLER_TYPE::LINEAR);
    ref objects_gpuprog = upload_gpuprogram(renderer, &bytecode);

    bytecode = compile_from_file(rayfmt_desc, "shaders/ray.vs", "shaders/ray.ps", SAMPLER_TYPE::NONE);
    ref rays_gpuprog = upload_gpuprogram(renderer, &bytecode);

    return objects_gpuprog;
}

ref prepare_lightmap(dx11renderer* renderer, lightmap* lm) {
    ref lightmap_ref = upload_lightmap(renderer, lm);
    if (lightmap_ref != invalid_ref) {
        set_lightmap(renderer, lightmap_ref);
    }

    return lightmap_ref;
}

std::vector<ref> prepare_meshes(dx11renderer* renderer, const static_geometry* geometry, const chart_projection* chart_proj) {
    // Prepare all meshes
    std::vector<ref> meshes;
    for (size_t i = 0; i < geometry->objects.name.size(); ++i) {
        mesh_asm obj_asm = assemble_object(geometry, chart_proj, i);
        ref mesh_ref = upload_mesh(renderer, &obj_asm);
        if (mesh_ref != invalid_ref) {
            meshes.push_back(mesh_ref);
        }
    }

    return meshes;
}

void clear_meshes(dx11renderer* renderer, std::vector<ref>* meshes) {
    for (auto& mesh : *meshes) {
            clear_mesh(renderer, mesh);
    }
}

lightmap solve_lightfield(const static_geometry* geometry, const chart_store* charts, chart_projection* chart_projections) {
    
    
    std::vector<chart_fragments> chart_fragments = create_fragments(geometry, chart_projections, charts);

    BVH bvh = construct(geometry);
    std::vector<chart_texture> textures = create_textures(&chart_fragments);

    for (U8 bounces = 0; bounces < 3; bounces++) {
        add_radiances(geometry, chart_projections, &chart_fragments, &textures, &bvh);
        //debug_out(&textures);
    }

    bilateral_filter(&textures, 5);
    //debug_out(&textures);

    lightmap lm = assemble_lightmap(&textures, chart_projections, 128);
    //debug_out(&lm);

    return lm;
}



//-----------------------------------------------------------------------------

int WINAPI WinMain(HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR lpCmdLine,
    int nCmdShow)
{    
    
     HWND main_window = open_window(L"Lightmapped");
     set_wheelfunctions(mousewheel_up, mousewheel_down);
     dx11renderer renderer = create_renderer(main_window);

     static_geometry geometry = {};
     custom_loader   loader;

     loader.Load(&geometry, "cornell_box.mesh");
     
     prim_adjacencies adjacencies = create_adjacencies(&geometry.prims);
     chartify_geometry(&geometry, &adjacencies);
     chart_store charts           = create_charts(&geometry, &adjacencies);
     chart_projection projections = local_project(&geometry, &charts, &adjacencies, 6.0f);
     lightmap lm                  = solve_lightfield(&geometry, &charts, &projections);

     ref gpu_prog = prepare_pipeline(&renderer);
     ref lm_ref = prepare_lightmap(&renderer, &lm);
     std::vector<ref> meshes = prepare_meshes(&renderer, &geometry, &projections);

     MSG msg = {};
     while (msg.message != WM_QUIT)
     {
         PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE);
         TranslateMessage(&msg);
         DispatchMessage(&msg);
         
         clear_target(&renderer);
     
         set_viewprojmatrix(&renderer, &view, &proj);
         set_gpuprogram(&renderer, gpu_prog);
     
         for (ref mesh_ref : meshes) {
             draw_mesh(&renderer, mesh_ref);
         };
     
         present_target(&renderer);
     }
     
     clear_meshes(&renderer, &meshes);
     clear_lightmap(&renderer, lm_ref);

     destroy_renderer(&renderer);
     close_window(main_window);
}