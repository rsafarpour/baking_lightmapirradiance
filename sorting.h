#pragma once
#include <vector>
#include <numeric>
#include <cassert>

using sort_key = std::vector<size_t>;

template <typename T, typename Compare>
sort_key record_sorting(const std::vector<T>* key, Compare comp) {
    sort_key sk = sort_key(key.size());
    std::iota(sk.begin(), sk.end());

    std::sort(sk.begin(), sk.end(),  comp);
    return sk;
}

template <typename T>
sort_key record_sorting(const std::vector<T>* key) {
    sort_key sk = sort_key(key->size());
    std::iota(sk.begin(), sk.end(), 0);

    std::sort(sk.begin(), sk.end(), [&](size_t i, size_t j) {return ((*key)[i] < (*key)[j]); });
    return sk;
}

template <typename T>
std::vector<T> get_sorted(const sort_key* sk, const std::vector<T>* values) {
    assert(sk->size() == values->size());
    std::vector<T> sorted(sk->size());
    std::transform(sk->begin(), sk->end(), sorted.begin(), [&](size_t i) {return (*values)[i]; });

    return sorted;
}
