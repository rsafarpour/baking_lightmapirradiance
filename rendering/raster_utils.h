#pragma once

#include "types.h"

inline void set_bounds(float a_u, float a_v, float b_u, float b_v, float c_u, float c_v, I32* tl_u, I32* tl_v, I32* br_u, I32* br_v)
{
    float tl_u_ = a_u;
    float tl_v_ = a_v;
    float br_u_ = a_u;
    float br_v_ = a_v;

    if (tl_u_ > b_u) { tl_u_ = (I32)b_u; }
    if (tl_u_ > c_u) { tl_u_ = (I32)c_u; }
    if (tl_v_ > b_v) { tl_v_ = (I32)b_v; }
    if (tl_v_ > c_v) { tl_v_ = (I32)c_v; }

    if (br_u_ < b_u) { br_u_ = (I32)b_u; }
    if (br_u_ < c_u) { br_u_ = (I32)c_u; }
    if (br_v_ < b_v) { br_v_ = (I32)b_v; }
    if (br_v_ < c_v) { br_v_ = (I32)c_v; }

    *tl_u = tl_u_;
    *tl_v = tl_v_;
    *br_u = br_u_ + 2; // Add 2 to have some space for gutter
    *br_v = br_v_ + 2; // Add 2 to have some space for gutter
}
