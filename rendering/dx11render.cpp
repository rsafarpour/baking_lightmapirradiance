#include "dx11render.h"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")

dx11renderer create_renderer(HWND output_window) {
    dx11renderer renderer;

    if (FAILED(CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&renderer.dxgi))) {
        MessageBox(NULL, TEXT("Failed initialising DXGI"), TEXT("Error"), MB_OK);
        return {};
    }
    
    if (FAILED(renderer.dxgi->EnumAdapters(0, &renderer.adapt))) {
        MessageBox(NULL, TEXT("Failed enumerating Adapters"), TEXT("Error"), MB_OK);
        return {};
    }

    D3D_FEATURE_LEVEL levels = { D3D_FEATURE_LEVEL_11_0 };
    DXGI_SWAP_CHAIN_DESC sc_desc               = {};
    sc_desc.BufferCount                        = 2;
    sc_desc.BufferDesc.Format                  = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
    sc_desc.BufferDesc.Height                  = 600;
    sc_desc.BufferDesc.RefreshRate.Denominator = 60;
    sc_desc.BufferDesc.RefreshRate.Numerator   = 1;
    sc_desc.BufferDesc.Scaling                 = DXGI_MODE_SCALING_UNSPECIFIED;
    sc_desc.BufferDesc.ScanlineOrdering        = DXGI_MODE_SCANLINE_ORDER_PROGRESSIVE;
    sc_desc.BufferDesc.Width                   = 800;
    sc_desc.BufferUsage                        = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sc_desc.Flags                              = 0;
    sc_desc.OutputWindow                       = output_window;
    sc_desc.SampleDesc.Count                   = 1;
    sc_desc.SampleDesc.Quality                 = 0;
    sc_desc.SwapEffect                         = DXGI_SWAP_EFFECT_DISCARD;
    sc_desc.Windowed                           = true;

    if (FAILED(D3D11CreateDeviceAndSwapChain(
        renderer.adapt,
        D3D_DRIVER_TYPE_UNKNOWN,
        nullptr,
        D3D11_CREATE_DEVICE_DEBUG,
        &levels,
        1,
        D3D11_SDK_VERSION,
        &sc_desc,
        &renderer.sc,
        &renderer.dev,
        &renderer.feature_level,
        &renderer.ctx))) {
        MessageBox(NULL, TEXT("Failed creating swap chain"), TEXT("Error"), MB_OK);
        return {};
    }
    

    // FIXED depth/stencil
    D3D11_DEPTH_STENCIL_DESC dsstate_desc = {};
    dsstate_desc.DepthEnable             = true;
    dsstate_desc.StencilEnable           = false;
    dsstate_desc.DepthFunc               = D3D11_COMPARISON_LESS;
    dsstate_desc.DepthWriteMask          = D3D11_DEPTH_WRITE_MASK_ALL;
    dsstate_desc.BackFace.StencilFailOp  = D3D11_STENCIL_OP_KEEP;
    dsstate_desc.BackFace.StencilPassOp  = D3D11_STENCIL_OP_KEEP;
    dsstate_desc.BackFace.StencilFunc    = D3D11_COMPARISON_ALWAYS;
    dsstate_desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    dsstate_desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    dsstate_desc.FrontFace.StencilFunc   = D3D11_COMPARISON_ALWAYS;
    renderer.dev->CreateDepthStencilState(&dsstate_desc, &renderer.ds_state);
    renderer.ctx->OMSetDepthStencilState(renderer.ds_state, 0);
    
    D3D11_TEXTURE2D_DESC dstexture_desc;
    dstexture_desc.ArraySize = 1;
    dstexture_desc.BindFlags = D3D10_BIND_DEPTH_STENCIL;
    dstexture_desc.CPUAccessFlags = 0;
    dstexture_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    dstexture_desc.Height = 600;
    dstexture_desc.MipLevels = 1;
    dstexture_desc.MiscFlags = 0;
    dstexture_desc.SampleDesc.Count = 1;
    dstexture_desc.SampleDesc.Quality = 0;
    dstexture_desc.Usage = D3D11_USAGE_DEFAULT;
    dstexture_desc.Width = 800;

    D3D11_DEPTH_STENCIL_VIEW_DESC dsv_desc;
    dsv_desc.Flags = 0;
    dsv_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    dsv_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    dsv_desc.Texture2D.MipSlice = 0;


    // FIXED blend & rasterizer state
    D3D11_BLEND_DESC blend_desc = {};
    blend_desc.AlphaToCoverageEnable = false;
    blend_desc.IndependentBlendEnable = false;
    blend_desc.RenderTarget[0].BlendEnable = true;
    blend_desc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    blend_desc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    blend_desc.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
    blend_desc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
    blend_desc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
    blend_desc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
    blend_desc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

    D3D11_RASTERIZER_DESC rasterizer_desc = {};
    rasterizer_desc.AntialiasedLineEnable = false;
    rasterizer_desc.CullMode = D3D11_CULL_NONE;
    rasterizer_desc.DepthBias = 0;
    rasterizer_desc.DepthBiasClamp = 0.0f;
    rasterizer_desc.DepthClipEnable = true;
    rasterizer_desc.FillMode = D3D11_FILL_SOLID;
    rasterizer_desc.FrontCounterClockwise = true;
    rasterizer_desc.MultisampleEnable = false;
    rasterizer_desc.ScissorEnable = false;
    rasterizer_desc.SlopeScaledDepthBias = 0.0f;


    // FIXED viewport
    D3D11_VIEWPORT vp = {};
    vp.TopLeftX = 0.0f;
    vp.TopLeftY = 0.0f;
    vp.Height   = 600.0f;
    vp.Width    = 800.0f;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;


    // Create bilinear sampler
    D3D11_SAMPLER_DESC sampler_desc = {};
    sampler_desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
    sampler_desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
    sampler_desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
    //sampler_desc.BorderColor    = {0.0f, 0.0f, 0.0f, 0.0f};
    sampler_desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
    sampler_desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
    sampler_desc.MaxAnisotropy = 1;
    //sampler_desc.MaxLOD         = 0;
    //sampler_desc.MinLOD         = 0;
    //sampler_desc.MipLODBias     = 0;

    ID3D11Texture2D* backbuffer;
    D3D11_RENDER_TARGET_VIEW_DESC rt;
    rt.ViewDimension        = D3D11_RTV_DIMENSION_TEXTURE2D;
    rt.Format               = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
    rt.Texture2D.MipSlice   = 0;



    if(FAILED(renderer.dev->CreateTexture2D(&dstexture_desc, nullptr, &renderer.ds_texture)) ||
       FAILED(renderer.dev->CreateDepthStencilView(renderer.ds_texture, &dsv_desc, &renderer.dsv)) ||
       FAILED(renderer.dev->CreateRasterizerState(&rasterizer_desc, &renderer.rasterizer_state)) ||
       FAILED(renderer.dev->CreateBlendState(&blend_desc, &renderer.blend_state)) ||
       FAILED(renderer.dev->CreateSamplerState(&sampler_desc, &renderer.bilinear_sampler)) || 
       FAILED(renderer.sc->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backbuffer)) ||
       FAILED(renderer.dev->CreateRenderTargetView(backbuffer, nullptr, &renderer.rtv))) {
        MessageBox(NULL, TEXT("Failed configuring DirectX"), TEXT("Error"), MB_OK);
        return {};
    }
    backbuffer->Release();

    renderer.ctx->OMSetRenderTargets(1, &renderer.rtv, renderer.dsv);
    renderer.ctx->OMSetBlendState(renderer.blend_state, nullptr, 0xFFFFFFFF);
    renderer.ctx->RSSetState(renderer.rasterizer_state);
    renderer.ctx->RSSetViewports(1, &vp);

    // Set up view matrix
    D3D11_BUFFER_DESC buffer_desc = {};
    buffer_desc.BindFlags           = D3D11_BIND_CONSTANT_BUFFER;
    buffer_desc.ByteWidth           = 2*sizeof(float4x4);
    buffer_desc.StructureByteStride = 0;
    buffer_desc.Usage               = D3D11_USAGE_DYNAMIC;
    buffer_desc.CPUAccessFlags      = D3D11_CPU_ACCESS_WRITE;

    if (FAILED(renderer.dev->CreateBuffer(&buffer_desc, nullptr, &renderer.vp_matrix))) {
        MessageBox(NULL, TEXT("Failed creating view matrix"), TEXT("Error"), MB_OK);
        return {};
    }

    // Matrix is fixed to VS slot 0
    renderer.ctx->VSSetConstantBuffers(0, 1, &renderer.vp_matrix);
    return renderer;
}


void destroy_renderer(dx11renderer* renderer) {
    if (renderer->ds_state         != nullptr) { renderer->ds_state->Release();         }
    if (renderer->blend_state      != nullptr) { renderer->blend_state->Release();      }
    if (renderer->rasterizer_state != nullptr) { renderer->rasterizer_state->Release(); }
    if (renderer->ds_texture       != nullptr) { renderer->ds_texture->Release();       }
    if (renderer->dsv              != nullptr) { renderer->dsv->Release();              }
    if (renderer->rtv              != nullptr) { renderer->rtv->Release();              }
    if (renderer->bilinear_sampler != nullptr) { renderer->bilinear_sampler->Release(); }
    if( renderer->vp_matrix        != nullptr) { renderer->vp_matrix->Release();        }

    renderer->dev->Release();
    renderer->ctx->Release();
    renderer->sc->Release();
    renderer->adapt->Release();
    renderer->dxgi->Release();

    for (auto prog : renderer->gpu_programs) {
        if (prog.layout != nullptr) { prog.layout->Release(); }
        if (prog.vs != nullptr) { prog.vs->Release(); }
        if (prog.ps != nullptr) { prog.ps->Release(); }
    }

    for (auto tex2d : renderer->textures2d) {
        if (tex2d.resource != nullptr) { tex2d.resource->Release(); }
        if (tex2d.view != nullptr) { tex2d.view->Release(); }
    }
}

void clear_target(dx11renderer* renderer) {
    FLOAT clear_color[] = { 0.0f, 0.0f, 0.0f, 0.0 };
    renderer->ctx->ClearRenderTargetView(renderer->rtv, clear_color);
    renderer->ctx->ClearDepthStencilView(renderer->dsv, D3D11_CLEAR_DEPTH , 1.0f, 0);
}

void present_target(dx11renderer* renderer) {
    renderer->sc->Present(1, 0);
}

void set_lightmap(dx11renderer* renderer, ref lm) {
    if (lm.value < renderer->textures2d.size()) {
        renderer->ctx->PSSetShaderResources(0, 1, &renderer->textures2d[lm.value].view);
    }
}


void set_viewprojmatrix(dx11renderer* renderer, float4x4* view_matrix, float4x4* proj_matrix) {

    float4x4 v = transposed(view_matrix);
    float4x4 p = transposed(proj_matrix);

    D3D11_MAPPED_SUBRESOURCE mapped_data;
    if (SUCCEEDED(renderer->ctx->Map(renderer->vp_matrix, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_data))) {
        memcpy(mapped_data.pData, &v, sizeof(v));
        memcpy((void*)(U64(mapped_data.pData) + sizeof(v)), &p, sizeof(p));
        renderer->ctx->Unmap(renderer->vp_matrix, 0);
    }
}

void set_gpuprogram(dx11renderer* renderer, ref program_ref) {
    if (program_ref.value < renderer->gpu_programs.size()) {
        renderer->ctx->VSSetShader(renderer->gpu_programs[program_ref.value].vs, nullptr, 0);
        renderer->ctx->PSSetShader(renderer->gpu_programs[program_ref.value].ps, nullptr, 0);
        renderer->ctx->IASetInputLayout(renderer->gpu_programs[program_ref.value].layout);
        renderer->ctx->PSSetSamplers(0, 1, &renderer->gpu_programs[program_ref.value].sampler_state);
    }
}

ref upload_mesh(dx11renderer* renderer, const mesh_asm* mesh) {

    ID3D11Buffer* vbuffer = nullptr;

    D3D11_BUFFER_DESC vbuffer_desc = {};
    vbuffer_desc.BindFlags           = D3D11_BIND_VERTEX_BUFFER;
    vbuffer_desc.ByteWidth           = mesh->vertex_bytes;
    vbuffer_desc.StructureByteStride = sizeof(custom_format);
    vbuffer_desc.Usage               = D3D11_USAGE_DEFAULT;

    D3D11_SUBRESOURCE_DATA vsub_data = {};
    vsub_data.pSysMem = mesh->vertex_data;



    ID3D11Buffer* ibuffer = nullptr;
    D3D11_BUFFER_DESC ibuffer_desc = {};
    ibuffer_desc.BindFlags           = D3D11_BIND_INDEX_BUFFER;
    ibuffer_desc.ByteWidth           = mesh->index_bytes;
    ibuffer_desc.StructureByteStride = sizeof(index_t);
    ibuffer_desc.Usage               = D3D11_USAGE_DEFAULT;

    D3D11_SUBRESOURCE_DATA isub_data = {};
    isub_data.pSysMem = mesh->index_data;

    if (FAILED(renderer->dev->CreateBuffer(&vbuffer_desc, &vsub_data, &vbuffer)) ||
        FAILED(renderer->dev->CreateBuffer(&ibuffer_desc, &isub_data, &ibuffer))) {
        return invalid_ref;
    }

    dx11mesh gpu_mesh = {
        mesh->topology,
        vbuffer,
        ibuffer,
        mesh->index_bytes/sizeof(index_t)
    };

    ref r;
    r.value      = renderer->meshes.size();
    r.generation = 0;
    renderer->meshes.push_back(gpu_mesh);

    return r;
}

ref upload_gpuprogram(dx11renderer* renderer, gpu_bytecode* code) {
    ID3D11VertexShader* vs = nullptr;
    ID3D11PixelShader*  ps = nullptr;
    if (FAILED(renderer->dev->CreateVertexShader(code->vs->GetBufferPointer(), code->vs->GetBufferSize(), nullptr, &vs)) ||
        FAILED(renderer->dev->CreatePixelShader(code->ps->GetBufferPointer(), code->ps->GetBufferSize(), nullptr, &ps))) {
        return  invalid_ref;
    }

    ID3D11InputLayout* input_layout;
    if (FAILED(renderer->dev->CreateInputLayout(
        code->input_layout.data(),
        code->input_layout.size(),
        code->vs->GetBufferPointer(),
        code->vs->GetBufferSize(),
        &input_layout))) {
        return  invalid_ref;
    }

    gpu_program program = {
        vs,
        ps,
        input_layout,
        code->sampler_type == SAMPLER_TYPE::LINEAR ? renderer->bilinear_sampler : nullptr
    };

    ref r = {};
    r.value = renderer->gpu_programs.size();

    renderer->gpu_programs.push_back(program);
    return r;
}

ref upload_lightmap(dx11renderer* renderer, const lightmap* lm) {
    D3D11_TEXTURE2D_DESC desc = {};
    desc.ArraySize          = 1;
    desc.BindFlags          = D3D11_BIND_SHADER_RESOURCE;
    desc.Format             = DXGI_FORMAT_R32G32B32A32_FLOAT;
    desc.Height             = lm->dims.y;
    desc.MipLevels          = 1;
    desc.SampleDesc.Count   = 1;
    desc.SampleDesc.Quality = 0;
    desc.Usage              = D3D11_USAGE_DEFAULT;
    desc.Width              = lm->dims.x;

    D3D11_SUBRESOURCE_DATA data = {};
    data.pSysMem = lm->data.data();
    data.SysMemPitch = lm->dims.x*sizeof(float4);

    dx11texture2d texture;
    if (FAILED(renderer->dev->CreateTexture2D(&desc, &data, &texture.resource))) {
        return invalid_ref;
    }
    
    D3D11_SHADER_RESOURCE_VIEW_DESC srv_desc = {};
    srv_desc.Format                    = DXGI_FORMAT_R32G32B32A32_FLOAT;
    srv_desc.Texture2D.MipLevels       = 1;
    srv_desc.Texture2D.MostDetailedMip = 0;
    srv_desc.ViewDimension             = D3D11_SRV_DIMENSION_TEXTURE2D;
    if (FAILED(renderer->dev->CreateShaderResourceView(texture.resource, &srv_desc, &texture.view))) {
        texture.resource->Release();
        return invalid_ref;
    }

    ref texture_ref;
    texture_ref.value      = renderer->textures2d.size();
    texture_ref.generation = 0;
    renderer->textures2d.push_back(texture);

    return texture_ref;
}

void clear_mesh(dx11renderer* renderer, ref mesh_ref) {
    if (mesh_ref.value < renderer->meshes.size()) {
        renderer->meshes[mesh_ref.value].vertex_buffer->Release();
        renderer->meshes[mesh_ref.value].index_buffer->Release();
    }
}
void clear_gpuprogram(dx11renderer* renderer, ref gpuprog_ref) {
    if (gpuprog_ref.value < renderer->gpu_programs.size()) {
        renderer->gpu_programs[gpuprog_ref.value].vs->Release();
        renderer->gpu_programs[gpuprog_ref.value].ps->Release();
        renderer->gpu_programs[gpuprog_ref.value].layout->Release();
        renderer->gpu_programs[gpuprog_ref.value] = {};
    }
}

void clear_lightmap(dx11renderer* renderer, ref lm_ref) {
    if (lm_ref.value < renderer->textures2d.size()) {
        renderer->textures2d[lm_ref.value].resource->Release();
        renderer->textures2d[lm_ref.value].view->Release();
        renderer->textures2d[lm_ref.value] = {};
    }
}

void draw_mesh(dx11renderer* renderer, ref mesh) {
    if (mesh.value < renderer->meshes.size()) {
        ID3D11Buffer* vbuffer[] = { renderer->meshes[mesh.value].vertex_buffer };
        ID3D11Buffer* ibuffer =  renderer->meshes[mesh.value].index_buffer;
        UINT stride[] = { sizeof(custom_format) };
        UINT offsets  = 0;
        renderer->ctx->IASetVertexBuffers(0, 1, vbuffer, stride, &offsets);
        renderer->ctx->IASetIndexBuffer(ibuffer, DXGI_FORMAT_R32_UINT, 0);
        renderer->ctx->IASetPrimitiveTopology(renderer->meshes[mesh.value].topology);

        ID3D11Buffer* b[] = { renderer->vp_matrix };
        renderer->ctx->VSSetConstantBuffers(0, 1, b);

        renderer->ctx->DrawIndexed(renderer->meshes[mesh.value].num_indices, 0, 0);
    }
}
