#pragma once

#define NOMINMAX
#include <types.h>
#include <d3d11.h>
#include "../lightmap/lightmap.h"
#include "../geometry/mesh_assembly.h"
#include "../matrixtypes.h"

enum class SAMPLER_TYPE {
    NONE,
    LINEAR
};

struct ref {
    U32 value;
    U32 generation;
};

constexpr ref invalid_ref {
    U32{~0U},
    U32{~0U}
};

inline bool operator!=(ref lhs, ref rhs) {
    return ((rhs.value != lhs.value) ||
        (rhs.generation != lhs.generation));
    
}

inline bool operator==(ref lhs, ref rhs) {
    return ((rhs.value != lhs.value) ||
        (rhs.generation != lhs.generation));

}

using dx11cbuffer = ID3D11Buffer*;

struct gpu_bytecode {
    ID3DBlob*                             vs;
    ID3DBlob*                             ps;
    std::vector<D3D11_INPUT_ELEMENT_DESC> input_layout;
    SAMPLER_TYPE                          sampler_type;
};

struct gpu_program {
    ID3D11VertexShader* vs;
    ID3D11PixelShader*  ps;
    ID3D11InputLayout*  layout;
    ID3D11SamplerState* sampler_state;
};

struct dx11mesh {
    topology_t    topology;
    ID3D11Buffer* vertex_buffer;
    ID3D11Buffer* index_buffer;
    U32           num_indices;
};

struct dx11texture2d {
    ID3D11Texture2D*          resource;
    ID3D11ShaderResourceView* view;
};

struct dx11renderer {
    IDXGIFactory*              dxgi;
    IDXGIAdapter*              adapt;
    IDXGISwapChain*            sc;
    ID3D11Device*              dev;
    ID3D11DeviceContext*       ctx;
    D3D_FEATURE_LEVEL          feature_level;
    ID3D11Texture2D*           ds_texture;
    ID3D11DepthStencilState*   ds_state;
    ID3D11DepthStencilView*    dsv;
    ID3D11SamplerState*        bilinear_sampler;

    ID3D11BlendState*          blend_state;
    ID3D11RasterizerState*     rasterizer_state;
    ID3D11RenderTargetView*    rtv;

    std::vector<dx11texture2d> textures2d;
    std::vector<dx11mesh>      meshes;
    std::vector<gpu_program>   gpu_programs;
    dx11cbuffer                vp_matrix;
};

dx11renderer create_renderer(HWND output_window);
void         destroy_renderer(dx11renderer* renderer);
void         clear_target(dx11renderer* renderer);
void         present_target(dx11renderer* renderer);
void         set_lightmap(dx11renderer* renderer, ref lm);
void         set_viewprojmatrix(dx11renderer* renderer, float4x4* view_matrix, float4x4* proj_matrix);
void         set_gpuprogram(dx11renderer* renderer, ref program_ref);
ref          upload_mesh(dx11renderer* renderer, const mesh_asm* mesh);
ref          upload_gpuprogram(dx11renderer* renderer, gpu_bytecode* code);
ref          upload_lightmap(dx11renderer* renderer, const lightmap* lm);
void         clear_mesh(dx11renderer* renderer, ref mesh_ref);
void         clear_gpuprogram(dx11renderer* renderer, ref gpuprog_ref);
void         clear_lightmap(dx11renderer* renderer, ref lm_ref);
void         draw_mesh(dx11renderer* renderer, ref mesh);

