#include "gpu_compiler.h"
#include "../files/file.h"

#include <d3dcompiler.h>

std::string load_code(const char* fname) {
    size_t filesize = filesize_a(fname);

    std::string content;
    content.resize(filesize + 1);
    U64 codesize = read_a(fname, &content, filesize);
    if (codesize == 0) {
        return {};
    }

    return content;
}

ID3DBlob* compile_vs(const char* name, const std::string* code) {
    ID3DBlob* blob;
    ID3DBlob* err_msg;
    if (FAILED(D3DCompile(
        code->c_str(),
        code->size(),
        name,
        nullptr,
        nullptr,
        "main",
        "vs_5_0",
        D3DCOMPILE_DEBUG,
        0,
        &blob,
        &err_msg)))
    {
        OutputDebugStringA((const char*)err_msg->GetBufferPointer());
        err_msg->Release();
        return {};
    }

    return blob;
}

ID3DBlob* compile_ps(const char* name, const std::string* code) {
    ID3DBlob* blob;
    ID3DBlob* err_msg;
    if (FAILED(D3DCompile(
        code->c_str(),
        code->size(),
        name,
        nullptr,
        nullptr,
        "main",
        "ps_5_0",
        D3DCOMPILE_DEBUG,
        0,
        &blob,
        &err_msg)))
    {
        OutputDebugStringA((const char*)err_msg->GetBufferPointer());
        err_msg->Release();
        return {};
    }

    return blob;
}

gpu_bytecode compile_from_file(vertex_desc format, const char* vs_fname, const char* ps_fname, SAMPLER_TYPE sampler_type) {
    std::string vs_code = load_code(vs_fname);
    std::string ps_code = load_code(ps_fname);

    gpu_bytecode code;
    code.vs = compile_vs(vs_fname, &vs_code);
    code.ps = compile_ps(ps_fname, &ps_code);

    code.input_layout;
    code.input_layout.resize(format.data_formats.size());
    code.sampler_type = sampler_type;
    for (size_t i = 0; i < format.data_formats.size(); ++i) {
        code.input_layout[i].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
        code.input_layout[i].Format            = format.data_formats[i];
        code.input_layout[i].InputSlot         = 0;
        code.input_layout[i].InputSlotClass    = D3D11_INPUT_PER_VERTEX_DATA;
        code.input_layout[i].SemanticIndex     = 0;
        code.input_layout[i].SemanticName      = format.semantics[i];
    }
    
    return code;
}
