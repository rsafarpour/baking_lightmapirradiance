cbuffer ProjMatrix : register(b0)
{
	float4x4 _view;
	float4x4 _proj;
}

struct VertexIn
{
    float3 position : POSITION;
};

struct VertexOut
{
    float4 position : SV_POSITION;
};

VertexOut main(VertexIn i, uint id :SV_VertexID)
{
	float4 position = float4(i.position, 1.0f);
    float4x4 m = mul(_proj, _view);

    VertexOut o;
	o.position = mul(m, position);
    return o;
}