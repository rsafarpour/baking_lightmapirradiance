cbuffer ProjMatrix : register(b0)
{
	float4x4 _view;
	float4x4 _proj;
}

struct VertexIn
{
    float3 position   : POSITION;
    float3 normal     : NORMAL;
    float3 albedo     : ALBEDO;
	float3 emit       : EMITTANCE;
	float2 lm_coords  : LM_COORDS;
};

struct VertexOut
{
    float3 albedo    : ALBEDO;
	float3 emit      : EMITTANCE;
	float2 lm_coords : LM_COORDS;
    float4 position  : SV_POSITION;
};

VertexOut main(VertexIn i)
{
    float4   p = float4(i.position, 1.0);
    float4x4 m = mul(_proj, _view);

    VertexOut o;
    o.albedo    = i.albedo;
    o.emit      = i.emit;
    o.lm_coords = i.lm_coords;
	o.position  = mul(m, p);

	return o;
}