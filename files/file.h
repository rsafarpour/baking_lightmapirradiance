#pragma once

#include "types.h"
#include <string>

U64 filesize_a(const char* filename);
U64 read_a(const char* filename, std::string* dst, U64 dst_size);
U64 write_b(const char* filename, const char* src, U64 src_size);
