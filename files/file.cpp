#define NOMINMAX

#include "file.h"
#include <stdio.h>
#include <Windows.h>
#include <string>

// TODO: Differentiate ASCII and binary read/write
U64 filesize_a(const char* filename)
{
    FILE* f = nullptr;
    errno_t err = fopen_s(&f, filename, "r");

    if (err == 0)
    {
        if(fseek(f, 0, SEEK_END) == 0)
        {
            long fsize = ftell(f);
            if (fsize != EOF) { 
                fclose(f);
                return fsize; 
            }
        }
    }

    fclose(f);
    OutputDebugStringA("PokeFilesize: Error accessing file: \"");
    OutputDebugStringA(filename);
    OutputDebugStringA("\"\n");
    return 0;
}

U64 read_a(const char* filename, std::string* dst, U64 dst_size)
{
    FILE* f = nullptr;
    errno_t err = fopen_s(&f, filename, "r");
    if (err != 0)
    { 
        OutputDebugStringA("ReadFile: Error opening file: \"");
        OutputDebugStringA(filename);
        OutputDebugStringA("\"\n");
        return 0; 
    }

    char* buffer = (char*)malloc(dst_size);
    U64 end = fread(buffer, sizeof(char), (size_t)dst_size, f);
    if (end <= dst_size) {
        buffer[end] = '\0';
    }
    *dst = buffer;
    free(buffer);

    fclose(f);
    return end;
}

U64 write_b(const char* filename, const char* src, U64 src_size)
{
    FILE* f = nullptr;
    errno_t err = fopen_s(&f, filename, "wb");
    if (err != 0)
    {
        OutputDebugStringA("WriteFile: Error opening file: \"");
        OutputDebugStringA(filename);
        OutputDebugStringA("\"\n");
        return 0;
    }

    U64 end = fwrite(src, 1, src_size, f);
    if (end != src_size)
    {
        OutputDebugStringA("WriteFile: Error writing file: \"");
        OutputDebugStringA(filename);
        OutputDebugStringA("\"\n");
        return 0;
    }

    fclose(f);
    return end;
}