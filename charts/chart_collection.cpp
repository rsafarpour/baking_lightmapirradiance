#include "../sorting.h"
#include "chart_collection.h"
#include <queue>
#include <cassert>

#define CHART_THRESHOLD 0.95f
#define insertatoffset(offset, container, obj) (container.insert(container.begin() + offset, obj))

bool contains(const prim_indices* prim_indices, vert_id vertex) {
    return (vertex == prim_indices->a ||
        vertex == prim_indices->b ||
        vertex == prim_indices->c);
}

// Assumes adjacencies to be sorted by prim_ids
void push_free_neighbors(std::queue<prim_id>* candidates, const chart_store* charts, const prim_adjacencies* adjacencies, prim_id prim) {
    auto iter = std::lower_bound(adjacencies->prim_ids.begin(), adjacencies->prim_ids.end(), prim);
    for (; iter != adjacencies->prim_ids.end() && *iter == prim; iter++) {
        ptrdiff_t index = iter - adjacencies->prim_ids.begin();

        prim_id neighbor = adjacencies->neighbor_ids[index];
        if (std::find(charts->prim_ids.begin(), charts->prim_ids.end(), neighbor) == charts->prim_ids.end()) {
            candidates->push(neighbor);
        }
    }
}

normal_t calc_primnormal(const static_geometry* geometry, prim_id prim) {
    const prim_indices* indices = &geometry->prims.faces[prim];
    float3 ab = geometry->verts.positions[indices->c] - geometry->verts.positions[indices->a];
    float3 ac = geometry->verts.positions[indices->b] - geometry->verts.positions[indices->a];
    float3 normal = normalised(cross(ab, ac));
    return normal;
}

normal_t calc_avgnormal(const chart_store* charts, chart_id chart, const static_geometry* geometry) {
    normal_t avg = { 0.0, 0.0, 0.0 };
    U64 num_normals = 0;
    for (U64 i = 0; i < charts->chart_ids.size(); ++i) {
        if (charts->chart_ids[i] == chart) {
            prim_id prim = charts->prim_ids[i];
            normal_t prim_normal = calc_primnormal(geometry, prim);
            avg.x += prim_normal.x;
            avg.y += prim_normal.y;
            avg.z += prim_normal.z;
            num_normals++;
        }
    }

    avg.x /= float(num_normals);
    avg.y /= float(num_normals);
    avg.z /= float(num_normals);
    avg = normalised(avg);

    return avg;
}

void free_edgevertices(static_geometry* geometry, prim_adjacencies* adjacencies, vert_id offset) {
    // All references to vertices with higher index
    // need to be adjusted
    for (prim_id pid = 0; pid < geometry->prims.faces.size(); ++pid) {
        geometry->prims.faces[pid].a += geometry->prims.faces[pid].a >= offset ? 2 : 0;
        geometry->prims.faces[pid].b += geometry->prims.faces[pid].b >= offset ? 2 : 0;
        geometry->prims.faces[pid].c += geometry->prims.faces[pid].c >= offset ? 2 : 0;
    }

    // Same for object bases
    for (object_id oid = 0; oid < geometry->objects.name.size(); ++oid) {
        geometry->objects.vert_base[oid] += geometry->objects.vert_base[oid] >= offset ? 2 : 0;
    }

    // Same for adjacencies shared edges
    for (edge_id eid = 0; eid < adjacencies->shared_edge.size(); ++eid) {
        adjacencies->shared_edge[eid].a += adjacencies->shared_edge[eid].a >= offset ? 2 : 0;
        adjacencies->shared_edge[eid].b += adjacencies->shared_edge[eid].b >= offset ? 2 : 0;
    }
}

void remove_adjacency(prim_adjacencies* adjacencies, edge_id eid) {
    prim_id pid = adjacencies->prim_ids[eid];
    prim_id nid = adjacencies->neighbor_ids[eid];

    adjacencies->shared_edge.erase(adjacencies->shared_edge.begin() + eid);
    adjacencies->prim_ids.erase(adjacencies->prim_ids.begin() + eid);
    adjacencies->neighbor_ids.erase(adjacencies->neighbor_ids.begin() + eid);

    // Find representation of reverse adjacency
    auto iter = std::lower_bound(adjacencies->prim_ids.begin(), adjacencies->prim_ids.end(), nid);
    assert(iter != adjacencies->prim_ids.end());
    eid = iter - adjacencies->prim_ids.begin();
    while (eid < adjacencies->prim_ids.size() && adjacencies->prim_ids[eid] == nid && adjacencies->neighbor_ids[eid] != pid) {
        eid++;
    }

    adjacencies->shared_edge.erase(adjacencies->shared_edge.begin() + eid);
    adjacencies->prim_ids.erase(adjacencies->prim_ids.begin() + eid);
    adjacencies->neighbor_ids.erase(adjacencies->neighbor_ids.begin() + eid);
}

void split_edge(static_geometry* geometry, prim_adjacencies* adjacencies, edge_id eid) {
    edge shared_edge = adjacencies->shared_edge[eid];

    // Identify edge's object because the new edge needs 
    // to be added to the same object as the old one
    prim_id offset = 0;
    object_id oid = 0;
        for (; oid < geometry->objects.name.size(); ++oid) {
        vert_id begin = geometry->objects.vert_base[oid];
        vert_id end = begin + geometry->objects.num_objverts[oid];

        if (shared_edge.a < end && shared_edge.b >= begin) { 
            offset = end;
            break;
        }
    }

    
    // Duplicate edge and insert at position
    vert_id a = shared_edge.a;
    vert_id b = shared_edge.b;

    vert_id new_a = offset;
    vert_id new_b = offset + 1;
    insertatoffset(offset, geometry->verts.albedo, geometry->verts.albedo[b]);
    insertatoffset(offset, geometry->verts.emittance, geometry->verts.emittance[b]);
    insertatoffset(offset, geometry->verts.normals, geometry->verts.normals[b]);
    insertatoffset(offset, geometry->verts.positions, geometry->verts.positions[b]);

    insertatoffset(offset, geometry->verts.albedo, geometry->verts.albedo[a]);
    insertatoffset(offset, geometry->verts.emittance, geometry->verts.emittance[a]);
    insertatoffset(offset, geometry->verts.normals, geometry->verts.normals[a]);
    insertatoffset(offset, geometry->verts.positions, geometry->verts.positions[a]);
    geometry->objects.num_objverts[oid] += 2;

    // Replace neighbors edge with new edge
    prim_id neighbor = adjacencies->neighbor_ids[eid];
    prim_indices indices = geometry->prims.faces[neighbor];
    indices.a = (indices.a == a) ? new_a : indices.a;
    indices.b = (indices.b == a) ? new_a : indices.b;
    indices.c = (indices.c == a) ? new_a : indices.c;
    indices.a = (indices.a == b) ? new_b : indices.a;
    indices.b = (indices.b == b) ? new_b : indices.b;
    indices.c = (indices.c == b) ? new_b : indices.c;

    free_edgevertices(geometry, adjacencies, offset);
    remove_adjacency(adjacencies, eid);

    geometry->prims.faces[neighbor] = indices;
}

void chartify_geometry(static_geometry* geometry, prim_adjacencies* adjacencies) {
    prim_store* prims = &geometry->prims;

    std::vector<edge_id> splits;
    for (prim_id prim = 0; prim < prims->faces.size(); ++prim) {
        normal_t pnormal = calc_primnormal(geometry, prim);

        auto iter = std::lower_bound(adjacencies->prim_ids.begin(), adjacencies->prim_ids.end(), prim);
        assert(iter != adjacencies->prim_ids.end());

        for (prim_id aid = iter - adjacencies->prim_ids.begin(); 
            aid < adjacencies->prim_ids.size() &&
            adjacencies->prim_ids[aid] == prim; 
            ++aid) {
            normal_t nnormal = calc_primnormal(geometry, adjacencies->neighbor_ids[aid]);
            if (dot(pnormal, nnormal) < CHART_THRESHOLD) {
                splits.push_back(aid);
            }
        }

    }
    for (auto split : splits) {
        split_edge(geometry, adjacencies, split);
    }

}

// Must return adjacencies sorted by prim_ids
prim_adjacencies create_adjacencies(const prim_store* prims) {

    prim_adjacencies adjacencies;
    for (U64 i = 0; i < prims->faces.size(); ++i) {
        for (U64 j = 0; j < prims->faces.size(); ++j) {

            if (i == j) {
                continue;
            }

            if (contains(&prims->faces[i], prims->faces[j].a)) {
                if (contains(&prims->faces[i], prims->faces[j].b)) {
                    adjacencies.prim_ids.push_back(i);
                    adjacencies.neighbor_ids.push_back(j);
                    adjacencies.shared_edge.push_back(edge{ prims->faces[j].a, prims->faces[j].b });
                }
                if (contains(&prims->faces[i], prims->faces[j].c)) {
                    adjacencies.prim_ids.push_back(i);
                    adjacencies.neighbor_ids.push_back(j);
                    adjacencies.shared_edge.push_back(edge{ prims->faces[j].a, prims->faces[j].c });
                }
            }
            if (contains(&prims->faces[i], prims->faces[j].b)) {
                if (contains(&prims->faces[i], prims->faces[j].c)) {
                    adjacencies.prim_ids.push_back(i);
                    adjacencies.neighbor_ids.push_back(j);
                    adjacencies.shared_edge.push_back(edge{ prims->faces[j].b, prims->faces[j].c });
                }
            }
        }
    }

    return adjacencies;
}

// Charts are being sorted by chart_id
chart_store create_charts(const static_geometry* geometry, const prim_adjacencies* adjacencies) {
    chart_id id = 0;
    chart_store charts = {};
    for (U64 i = 0; i < geometry->prims.num_prims; ++i) {

        // Skip face if it was added to a chart already
        auto membership = std::find(charts.prim_ids.begin(), charts.prim_ids.end(), i);
        if (membership == charts.prim_ids.end()) {
            // Start a new chart and add face to it
            charts.chart_ids.push_back(id);
            charts.prim_ids.push_back(i);

            // Push all face neighbors to candidate queue, then iterate: 
            // a) Pop from candidate from queue
            // b) If face's normal dotted with average chart normal 
            //    is above a threshold add candidate to chart
            std::queue<prim_id> candidates;
            push_free_neighbors(&candidates, &charts, adjacencies, i);
            while (candidates.size() > 0) {
                prim_id candidate = candidates.front();
                normal_t avg_normal = calc_avgnormal(&charts, id, geometry);
                normal_t prim_normal = calc_primnormal(geometry, candidate);
                if (dot(avg_normal, prim_normal) > CHART_THRESHOLD) {
                    charts.chart_ids.push_back(id);
                    charts.prim_ids.push_back(candidate);
                    push_free_neighbors(&candidates, &charts, adjacencies, candidate);
                    candidates.pop();
                }
            }

            id++;
        }
    }

    charts.num_charts = id;
    return charts;
}

// Assumes charts to be sorted by chart_ids
chart_projection local_project(const static_geometry* geometry, const chart_store* charts, const prim_adjacencies* adjacencies, float scale) {

    chart_projection projections;
    projections.charts.resize(geometry->verts.positions.size());
    projections.rawlm_coords.resize(geometry->verts.positions.size());

    size_t num_prims = charts->prim_ids.size();
    size_t j = 0;
    for (size_t i = 0; i < charts->num_charts; ++i) {
        vert_id id_a = geometry->prims.faces[charts->prim_ids[j]].a;
        vert_id id_b = geometry->prims.faces[charts->prim_ids[j]].b;
        vert_id id_c = geometry->prims.faces[charts->prim_ids[j]].c;

        float3 origin = geometry->verts.positions[id_a];
        float3 x_axis = normalised(geometry->verts.positions[id_b] - geometry->verts.positions[id_a]);
        float3 y_axis = normalised(geometry->verts.positions[id_c] - geometry->verts.positions[id_a]);
        orthogonalise(&x_axis, &y_axis);

        for (; j < num_prims && charts->chart_ids[j] == i; ++j) {
            vert_id id_a = geometry->prims.faces[charts->prim_ids[j]].a;
            vert_id id_b = geometry->prims.faces[charts->prim_ids[j]].b;
            vert_id id_c = geometry->prims.faces[charts->prim_ids[j]].c;

            projections.charts[id_a] = i;
            projections.rawlm_coords[id_a] = { scale*dot(x_axis, geometry->verts.positions[id_a] - origin),
                                               scale*dot(y_axis, geometry->verts.positions[id_a] - origin) };
            projections.charts[id_b] = i;
            projections.rawlm_coords[id_b] = { scale*dot(x_axis, geometry->verts.positions[id_b] - origin),
                                               scale*dot(y_axis, geometry->verts.positions[id_b] - origin) };
            projections.charts[id_c] = i;
            projections.rawlm_coords[id_c] = { scale*dot(x_axis, geometry->verts.positions[id_c] - origin),
                                               scale*dot(y_axis, geometry->verts.positions[id_c] - origin) };
        }
    }

    chart_bounds bounds = calc_bounds(&projections, charts);
    for (size_t i = 0; i < projections.rawlm_coords.size(); ++i) {
        float2 offset = {
                1.0 + bounds.minima[projections.charts[i]].x < 0.0f ? -bounds.minima[projections.charts[i]].x : 0.0f, // Adding 1.0 margin to top and left
                1.0 + bounds.minima[projections.charts[i]].y < 0.0f ? -bounds.minima[projections.charts[i]].y : 0.0f  // Adding 1.0 margin to top and left
        };
        projections.rawlm_coords[i] += offset;
    }

    return projections;
}

chart_bounds calc_bounds(const chart_projection* projection, const chart_store* charts) {
    chart_bounds bounds;
    bounds.minima.resize(charts->num_charts, { FLT_MAX,  FLT_MAX });
    bounds.maxima.resize(charts->num_charts, { -FLT_MAX, -FLT_MAX });
    for (size_t i = 0; i < projection->charts.size(); ++i) {
        chart_id cid = projection->charts[i];
        bounds.minima[cid] = min(bounds.minima[cid], projection->rawlm_coords[i]);
        bounds.maxima[cid] = max(bounds.maxima[cid], projection->rawlm_coords[i]);
    }

    return bounds;
}