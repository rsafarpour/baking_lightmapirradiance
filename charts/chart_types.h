#pragma once

#include "../rasterising/fragment.h"
#include <vector>

struct chart_fragments {
    std::vector<fragment> fragments;
    uint2                 dims;
};