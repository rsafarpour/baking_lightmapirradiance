#pragma once

#include "../geometry/static_geometry.h"
#include <vector>

using chart_id = U64;
using chart_prims = std::vector<std::vector<prim_id>>;

struct edge {
    vert_id a;
    vert_id b;
};

struct chart_bounds {
    std::vector<float2> minima;
    std::vector<float2> maxima;
};

struct chart_store {
    std::vector<chart_id> chart_ids;
    std::vector<prim_id>  prim_ids;
    size_t                num_charts;
};

struct prim_adjacencies {
    std::vector<prim_id>  prim_ids;
    std::vector<prim_id>  neighbor_ids;
    std::vector<edge>     shared_edge;
};

struct chart_projection {
    std::vector<chart_id>   charts;
    std::vector<texcoord_t> rawlm_coords;
};

void             chartify_geometry(static_geometry* geometry, prim_adjacencies* adjacencies);
prim_adjacencies create_adjacencies(const prim_store* prims);
chart_store      create_charts(const static_geometry* geometry, const prim_adjacencies* adjacencies);
chart_projection local_project(const static_geometry* geometry, const chart_store* charts, const prim_adjacencies* adjacencies, float scale);
chart_bounds     calc_bounds(const chart_projection* projection, const chart_store* charts);