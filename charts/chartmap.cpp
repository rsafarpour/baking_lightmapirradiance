#include "chartmap.h"
#include <cassert>
#include <string>
#include "../bitmap/bitmap.h"
#include "../sampling/sample.h"
#include "../geometry/raycast.h"

constexpr float Pi = 3.14159265f;

std::vector<uint2> calc_dims(const chart_bounds* bounds) {
    std::vector<uint2> dims(bounds->minima.size());
    for (size_t i = 0; i < bounds->minima.size(); ++i) {
        float2 dimf = bounds->maxima[i] - bounds->minima[i] + float2{0.5f, 0.5f};
        dims[i] = { unsigned(dimf.x), unsigned(dimf.y) };
    }

    return dims;
}

void dilate_fragment(std::vector<fragment>* frags, fragment f, int2 dir) {
    f.coord.x += dir.x;
    f.coord.y += dir.y;

    auto iter = std::lower_bound(frags->begin(), frags->end(), f, frag_coord_less);
    if (iter == frags->end() || iter->coord != f.coord) {
        frags->insert(iter, f);
    }
}

void clip_x(chart_projection* projections, chart_id cid) {
    for (size_t i = 0; i < projections->charts.size(); ++i) {
        if (projections->charts[i] == cid) {
            projections->rawlm_coords[i].x = floor(projections->rawlm_coords[i].x) + 0.5;
        }
    }
}

void clip_y(chart_projection* projections, chart_id cid) {
    for (size_t i = 0; i < projections->charts.size(); ++i) {
        if (projections->charts[i] == cid) {
            projections->rawlm_coords[i].y = floor(projections->rawlm_coords[i].y) + 0.5;
        }
    }
}

void dilate_charts(std::vector<chart_fragments>* fragment_sets, chart_projection* projections, const chart_bounds* bounds) {
    for (chart_id cid = 0; cid < fragment_sets->size(); ++cid) {
        auto& fragments = (*fragment_sets)[cid].fragments;
        float2 dims = bounds->maxima[cid] - bounds->minima[cid];

        if (dims.x < 1.0f) { clip_x(projections, cid); }
        if (dims.y < 1.0f) { clip_y(projections, cid); }

        std::vector<fragment> dilation_fragments;
        for (auto& f : (*fragment_sets)[cid].fragments) {

            if (dims.x >= 1.0f) {
                dilate_fragment(&dilation_fragments, f, { 1,0 });
                dilate_fragment(&dilation_fragments, f, { -1,0 });
            }

            if (dims.y >= 1.0f) {
                dilate_fragment(&dilation_fragments, f, { 0,1 });
                dilate_fragment(&dilation_fragments, f, { 0,-1 });
            }

            if (dims.x >= 1.0f && dims.y >= 0) {
                dilate_fragment(&dilation_fragments, f, { 1,1 });
                dilate_fragment(&dilation_fragments, f, { 1,-1 });
                dilate_fragment(&dilation_fragments, f, { -1,1 });
                dilate_fragment(&dilation_fragments, f, { -1,-1 });
            }
        }
    
        for (auto& f : dilation_fragments) {
            auto iter = std::lower_bound(fragments.begin(), fragments.end(), f, frag_coord_less);
            if (iter == fragments.end() || iter->coord != f.coord) {
                fragments.insert(iter, f);
            }
        }
    }
}

std::vector<chart_fragments> create_fragments(const static_geometry* geometry, chart_projection* projections, const chart_store* charts) {
    chart_bounds bounds = calc_bounds(projections, charts);

    // Offset all lightmap coordinates by 1 to add a gutter on the left and the top:
    for (auto& lm_coord : projections->rawlm_coords) {
        lm_coord += float2{ 1.0f, 1.0f };
    }

    std::vector<chart_fragments> fragment_sets(charts->num_charts);

    // Set all charts dimensions
    for (chart_id cid = charts->chart_ids[0]; cid < charts->num_charts; cid++) {
        float2 dimf = bounds.maxima[cid] - bounds.minima[cid] + float2{ 2.5f, 2.5f }; // Offset by 2 for gutter at all sides
        fragment_sets[cid].dims = { unsigned(dimf.x), unsigned(dimf.y) };
    }


    for (size_t i = 0; i < charts->chart_ids.size(); ++i) {
        prim_id pid = charts->prim_ids[i];
        chart_id cid = charts->chart_ids[i];
        prim_indices indices = geometry->prims.faces[pid];

        // Interpolate world space normals and positions, 
        // needed for a later ray trace step
        prop_t props[]{
            { geometry->verts.normals[indices.a],
              geometry->verts.normals[indices.b],
              geometry->verts.normals[indices.c]
            },
            { geometry->verts.positions[indices.a],
              geometry->verts.positions[indices.b],
              geometry->verts.positions[indices.c]
            },
            { geometry->verts.emittance[indices.a],
              geometry->verts.emittance[indices.b],
              geometry->verts.emittance[indices.c]
            },
        };

        texcoord_t lm_position[] = {
            projections->rawlm_coords[indices.a],
            projections->rawlm_coords[indices.b],
            projections->rawlm_coords[indices.c]
        };

        rasterise_primitive(&fragment_sets[cid].fragments, lm_position, props);
    }

    dilate_charts(&fragment_sets, projections, &bounds);
    return fragment_sets;
}

void interpolant_to_textures(const std::vector<chart_fragments>* chart_fragments, size_t prop_id, std::vector<chart_texture>* textures) {
    for (size_t i = 0; i < textures->size(); ++i) {
        uint2 dims = (*textures)[i].dims;
        for (auto& f : (*chart_fragments)[i].fragments) {
            float3 val = {};
            switch (prop_id) {
                case 0: val = f.position;  break;
                case 1: val = f.normal;    break;
                case 2: val = f.emittance; break;
            }


            (*textures)[i].data[3 * (f.coord.x + dims.x*f.coord.y) + 0] = val.x;
            (*textures)[i].data[3 * (f.coord.x + dims.x*f.coord.y) + 1] = val.y;
            (*textures)[i].data[3 * (f.coord.x + dims.x*f.coord.y) + 2] = val.z;
        }
    }
}

void emitted_radiance(const static_geometry* geometry, const std::vector<raycast_result_t>* results, std::vector<radiance_t>* radiances) {
    for (size_t i = 0; i < results->size(); ++i) {
        vert_id id_a = geometry->prims.faces[(*results)[i].prim].a;
        vert_id id_b = geometry->prims.faces[(*results)[i].prim].b;
        vert_id id_c = geometry->prims.faces[(*results)[i].prim].c;

        (*radiances)[i] += scale((*results)[i].bary_intersection.x, geometry->verts.emittance[id_a]) +
                           scale((*results)[i].bary_intersection.y, geometry->verts.emittance[id_b]) +
                           scale((*results)[i].bary_intersection.z, geometry->verts.emittance[id_c]);
    }
}

void reflected_radiance(const static_geometry* geometry, const std::vector<raycast_result_t>* results, const chart_projection* projections, std::vector<chart_texture>* textures, std::vector<radiance_t>* radiances) {
    for (size_t i = 0; i < results->size(); ++i) {
        vert_id id_a = geometry->prims.faces[(*results)[i].prim].a;
        vert_id id_b = geometry->prims.faces[(*results)[i].prim].b;
        vert_id id_c = geometry->prims.faces[(*results)[i].prim].c;


        chart_id cid = projections->charts[id_a];

        texcoord_t uv = scale((*results)[i].bary_intersection.x, projections->rawlm_coords[id_a]) +
                        scale((*results)[i].bary_intersection.y, projections->rawlm_coords[id_b]) +
                        scale((*results)[i].bary_intersection.z, projections->rawlm_coords[id_c]);

        float4 sampled = sample(&(*textures)[cid], uv);
        (*radiances)[i] += radiance_t{ sampled.x, sampled.y, sampled.z };
    }
}

void apply_lambert(const static_geometry* geometry, const std::vector<raycast_result_t>* results, std::vector<radiance_t>* radiances) {
    for (size_t i = 0; i < results->size(); ++i) {
        vert_id id_a = geometry->prims.faces[(*results)[i].prim].a;
        vert_id id_b = geometry->prims.faces[(*results)[i].prim].b;
        vert_id id_c = geometry->prims.faces[(*results)[i].prim].c;
        albedo_t albedo_a = geometry->verts.albedo[id_a];
        albedo_t albedo_b = geometry->verts.albedo[id_b];
        albedo_t albedo_c = geometry->verts.albedo[id_c];
        albedo_t albedo_interp = scale((*results)[i].bary_intersection.x, albedo_a)
                               + scale((*results)[i].bary_intersection.y, albedo_b)
                               + scale((*results)[i].bary_intersection.z, albedo_c);
        (*radiances)[i] = had(albedo_interp, scale(1.0f/Pi, (*radiances)[i]));
    }
}

void cosine_weight(const static_geometry* geometry, const std::vector<raycast_result_t>* results, const std::vector<float3>* rays, std::vector<radiance_t>* radiances) {
    for (size_t i = 0; i < rays->size(); ++i) {
        vert_id id_a = geometry->prims.faces[(*results)[i].prim].a;
        vert_id id_b = geometry->prims.faces[(*results)[i].prim].b;
        vert_id id_c = geometry->prims.faces[(*results)[i].prim].c;
        normal_t normal_a = geometry->verts.normals[id_a];
        normal_t normal_b = geometry->verts.normals[id_b];
        normal_t normal_c = geometry->verts.normals[id_c];
        normal_t normal_interp = scale((*results)[i].bary_intersection.x, normal_a)
                               + scale((*results)[i].bary_intersection.y, normal_b)
                               + scale((*results)[i].bary_intersection.z, normal_c);
        (*radiances)[i] = scale(dot(normal_interp, -(*rays)[i]), (*radiances)[i]);
    }
}

radiance_t montecarlo_integrate(std::vector<radiance_t>* radiances, size_t n_samples) {
    radiance_t result = {};
    for (radiance_t r : *radiances) {
        result += r;
    }

    result = scale((2*Pi)/float(n_samples), result);
    return result;
}

void add_radiances(const static_geometry* geometry, const chart_projection* projections, const std::vector<chart_fragments>* fragments, std::vector<chart_texture>* textures) {
    std::vector<chart_texture> new_textures = *textures;
    
    constexpr unsigned nrays = 128;
    for (size_t i = 0; i < fragments->size(); ++i){
        for (const fragment& f : (*fragments)[i].fragments) {
            std::vector<float3> rays = hemispherical_directions(&f.normal, nrays);
            std::vector<raycast_result_t> results = raycast(f.position, &rays, geometry);
            
            std::vector<radiance_t> radiances(results.size());
            reflected_radiance(geometry, &results, projections, textures, &radiances);
            apply_lambert(geometry, &results, &radiances);
            cosine_weight(geometry, &results, &rays, &radiances);

            emitted_radiance(geometry, &results, &radiances);

            radiance_t radiance = montecarlo_integrate(&radiances, nrays);
            float4 texture_value = { radiance.x, radiance.y, radiance.z, 1.0 };
            store(&new_textures[i], f.coord, texture_value);
        }
    }

    *textures = std::move(new_textures);
}

void add_radiances(const static_geometry* geometry, const chart_projection* projections, const std::vector<chart_fragments>* fragments, std::vector<chart_texture>* textures, const BVH* bvh) {
    std::vector<chart_texture> new_textures = *textures;

    constexpr unsigned nrays = 128;
    for (size_t i = 0; i < fragments->size(); ++i) {
        for (const fragment& f : (*fragments)[i].fragments) {
            std::vector<float3> rays = hemispherical_directions(&f.normal, nrays);
            std::vector<raycast_result_t> cmp = raycast(f.position, &rays, geometry);
            std::vector<raycast_result_t> results = raycast(f.position, &rays, geometry, bvh);

            std::vector<radiance_t> radiances(results.size());
            reflected_radiance(geometry, &results, projections, textures, &radiances);
            apply_lambert(geometry, &results, &radiances);
            cosine_weight(geometry, &results, &rays, &radiances);

            emitted_radiance(geometry, &results, &radiances);

            radiance_t radiance = montecarlo_integrate(&radiances, nrays);
            float4 texture_value = { radiance.x, radiance.y, radiance.z, 1.0 };
            store(&new_textures[i], f.coord, texture_value);
        }
    }

    *textures = std::move(new_textures);
}

float gauss_weight1D(float dist) {
    return (0.39894f*exp(-(dist*dist) / 2.0f));
}

float gauss_weight2D(float dist_x, float dist_y) {
    return gauss_weight1D(dist_x)*gauss_weight1D(dist_y);
}

void bilateral_filter(std::vector<chart_texture>* textures, I32 filter_width) {
    for (chart_texture& tex : *textures) { // For each chart
        chart_texture filtered = tex;
        for (size_t j = 0; j < tex.dims.y; ++j) { // Iterate over each lumel
            for (size_t i = 0; i < tex.dims.x; ++i) {

                if (load(&tex, { i,j }).w == 1.0) {
                    float n_samples = 0.0f;
                    float4 accum = {};
                    for (U64 t = std::max(0, I32(j) - filter_width); t < std::min(j + filter_width, tex.dims.y); ++t) { // Iterate over filter kernel
                        for (U64 s = std::max(0, I32(i) - filter_width); s < std::min(i + filter_width, tex.dims.x); ++s) {
                            float4 sample_val = load(&tex, { s, t });
                            n_samples += sample_val.w;
                            float dist_x = std::abs(float(i - s));
                            float dist_y = std::abs(float(j - t));
                            float weight = sample_val.w*gauss_weight2D(dist_x, dist_y);
                            accum += scale(sample_val.w, sample_val); // w == 0.0f marks vacant lumels
                        }
                    }

                    store(&filtered, { i,j }, scale(1.0f / n_samples, accum));
                }
            }
        }

        tex = std::move(filtered);
    }
}