#pragma once

#include "../geometry/bvh.h"
#include "../geometry/static_geometry.h"
#include "../rasterising/fragment.h"
#include "../rasterising/rasterise.h"
#include "../textures/textures.h"
#include "chart_collection.h"
#include "chart_types.h"

std::vector<chart_fragments> create_fragments(const static_geometry* geometry, chart_projection* projections, const chart_store* charts);
void                         add_radiances(const static_geometry* geometry, const chart_projection* projections, const std::vector<chart_fragments>* fragments, std::vector<chart_texture>* textures);
void                         add_radiances(const static_geometry* geometry, const chart_projection* projections, const std::vector<chart_fragments>* fragments, std::vector<chart_texture>* textures, const BVH* bvh);
void                         interpolant_to_textures(const std::vector<chart_fragments>* chart_fragments, size_t prop_id, std::vector<chart_texture>* textures);
void                         bilateral_filter(std::vector<chart_texture>* textures, I32 filter_width);