#pragma once

#include "fragment.h"
#include "../geometry/geometry_types.h"
#include <vector>

void rasterise_primitive(std::vector<fragment>* fragments, texcoord_t tex_coords[3], prop_t props[3]);