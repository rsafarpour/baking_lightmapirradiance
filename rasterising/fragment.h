#pragma once

#include "../vectortypes.h"

using prop_t = float3[3];

struct fragment {
    uint2  coord;
    float3 normal;
    float3 position;
    float3 emittance;
};

inline auto frag_coord_less = [](const fragment& lhs, const fragment& rhs) {
    if (lhs.coord.y == rhs.coord.y) {
        return (lhs.coord.x < rhs.coord.x);
    }
    return (lhs.coord.y < rhs.coord.y);
};