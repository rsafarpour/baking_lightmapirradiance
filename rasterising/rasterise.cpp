#include "rasterise.h"

void tri_setup(float2 coord, texcoord_t tex_coords[3], float dx[3], float dy[3], float e[3], float* dbl_area) {
    dx[0] = -(tex_coords[1].y - tex_coords[0].y); dy[0] = (tex_coords[1].x - tex_coords[0].x);
    dx[1] = -(tex_coords[2].y - tex_coords[1].y); dy[1] = (tex_coords[2].x - tex_coords[1].x);
    dx[2] = -(tex_coords[0].y - tex_coords[2].y); dy[2] = (tex_coords[0].x - tex_coords[2].x);
    e[0] = (dx[0] * (coord.x - tex_coords[0].x)) + (dy[0] * (coord.y - tex_coords[0].y));
    e[1] = (dx[1] * (coord.x - tex_coords[1].x)) + (dy[1] * (coord.y - tex_coords[1].y));
    e[2] = (dx[2] * (coord.x - tex_coords[2].x)) + (dy[2] * (coord.y - tex_coords[2].y));

    float2 baseline = tex_coords[1] - tex_coords[0];
    float2 normal_baseline = normalised(float2{ -baseline.y, baseline.x });

    float len_baseline = mag(baseline);
    float height = std::abs(dot(tex_coords[2] - tex_coords[0], normal_baseline));
    *dbl_area = len_baseline*height;
}

bool is_inside(float e, float dx, float dy) {
    if ( e > 0.0f) { return true;  }
    if ( e < 0.0f) { return false; }
    if (dx > 0.0f) { return true;  }
    if (dx < 0.0f) { return false; }
    if (dy > 0.0f) { return true;  }
    return false;
}

// Props are limited to three component vectors
void rasterise_primitive(std::vector<fragment>* fragments, texcoord_t tex_coords[3], prop_t props[3]) {
    // Rasteriser uses the simple scanline method, a bounding box
    // is needed to determine the area to scan through
    float2 bb_min = min(min(tex_coords[0], tex_coords[1]), tex_coords[2]);
    float2 bb_max = max(max(tex_coords[0], tex_coords[1]), tex_coords[2]);
    
    float2 sample_coord = { std::floor(bb_min.x) + 0.5f, std::floor(bb_min.y) + 0.5f }; // Samples lie at pixel centre
    float dx[] = { 0.0f, 0.0f, 0.0f };
    float dy[] = { 0.0f, 0.0f, 0.0f };
    float e[]  = { 0.0f, 0.0f, 0.0f };
    float dbl_area = 0.0f;
    tri_setup(sample_coord, tex_coords, dx, dy, e, &dbl_area);

    // Set up iteration variables and invariants
    I8 x_dir = 1;
    // + float2{2.0f, 2.0f} needed to match chartmap offset (guarantees a one px
    // gutter to keep the filter footprint clean )
    float2 dimf       = (bb_max - bb_min) + float2{2.0f, 2.0f}; 
    uint2  dims       = { unsigned(dimf.x), unsigned(dimf.y)};    
    uint2  frag_coord = { unsigned(bb_min.x), unsigned(bb_min.y)};

    // Use zig-zag scan line algorithm, also 
    // interpolate attached properties
    for (unsigned j = 0; j < dims.y; ++j) {
        float e_y[] = {e[0], e[1], e[2]};
        for (unsigned i = 0; i < dims.x; ++i) {
            if (is_inside(e[0], dx[0], dy[0]) &&
                is_inside(e[1], dx[1], dy[1]) &&
                is_inside(e[2], dx[2], dy[2])) {
                
                float coeff[] = {
                    e[0] / dbl_area,
                    e[1] / dbl_area,
                    e[2] / dbl_area
                };

                auto p1 = props[0][0];
                auto p2 = props[1][0];

                fragment f;
                f.coord = frag_coord;
                f.position = scale(coeff[1], props[1][0])
                            + scale(coeff[2], props[1][1])
                            + scale(coeff[0], props[1][2]);
                f.normal = scale(coeff[1], props[0][0])
                          + scale(coeff[2], props[0][1])
                          + scale(coeff[0], props[0][2]);
                f.emittance = scale(coeff[1], props[2][0])
                             + scale(coeff[2], props[2][1])
                             + scale(coeff[0], props[2][2]);
                auto iter = std::lower_bound(fragments->begin(), fragments->end(), f, frag_coord_less);
                fragments->insert(iter, f);
            }

            frag_coord.x++;
            e[0] += dx[0];
            e[1] += dx[1];
            e[2] += dx[2];
        }



        // Advance by line derivative
        frag_coord.x = unsigned(bb_min.x);
        e[0] = e_y[0] + dy[0];
        e[1] = e_y[1] + dy[1];
        e[2] = e_y[2] + dy[2];
        frag_coord.y++;
    }
}